> import Sound.SC3 {- hsc3 -}
> import qualified Sound.SC3.Common.Buffer as SC3 {- hsc3 -}

> import Sound.SC3.Lang.Pattern {- hsc3-lang -}

> import qualified Sound.SC3.Lang.Collection as Collection {- hsc3-lang -}
> import qualified Sound.SC3.Lang.Random.Gen as Gen {- hsc3-lang -}

`P_Event`s are `Audible`, if `scsynth` is running at port `57110` we
can hear an arpeggio.

> p_01 = pbind [(K_instr,psynth defaultSynthdef)
>              ,(K_degree,toP [0,2,4,7])
>              ,(K_dur,0.25)]

> p_02 =
>   let d = pseq [pshuf 'α' [-7,-3,0,2,4,7] 2
>                ,pseq [0,1,2,3,4,5,6,7] 1] 1
>       p = pbind [(K_dur,0.15),(K_degree,d)]
>       t n = padd (K_mtranspose,n) p
>   in pseq [p,t 1,t 2] inf

> p_03 =
>   pbind [(K_freq,pseq [440,550,660,770] 2)
>         ,(K_dur,pseq [0.1,0.15,0.1] inf)
>         ,(K_amp,pseq [0.1,0.05] inf)
>         ,(K_param "pan",pseq [-1,0,1] inf)]

'Sound.SC3.Lang.Control.Pitch.Pitch' can be written using midi note
numbers, 'K_midinote':

    > > Pbind(\dur,0.125,
    > >       \legato,0.2,
    > >       \midinote,Pseq(#[60,62,64,65,67,69,71,72],inf)).play

> p_04 = pbind [(K_dur,0.125)
>              ,(K_legato,0.2)
>              ,(K_midinote,pseq [60,62,64,65,67,69,71,72] inf)]

or frequencies in hertz, 'K_freq':

    > > Pbind(\dur,0.25,
    > >       \freq,Pseq(#[300,400,500,700,900],inf)).play

> p_05 = pbind [(K_dur,0.25)
>              ,(K_freq,pseq [300,400,500,700,900] inf)]

or as scale degrees, 'K_degree':

    > > Pbind(\degree,Pseq([Pshuf(#[-7,-3,0,2,4,7],4),
    > >                     Pseq([0,1,2,3,4,5,6,7])],inf),
    > >       \dur,0.15).play

> p_06 =
>   let d = pseqr (\e -> [pshuf e [-7,-3,0,2,4,7] 4
>                        ,pseq [0,1,2,3,4,5,6,7] 1]) inf
>   in pbind [(K_degree,d),(K_dur,0.15)]

Detuning in hertz is at 'K_detune':

    > > Pbind(\dur,0.25,
    > >       \detune,-20,
    > >       \freq,Pseq(#[300,400,500,700,900],inf)).play

> p_07 = pbind [(K_dur,0.25)
>              ,(K_detune,-20)
>              ,(K_freq,pseq [300,400,500,700,900] inf)]

Chords:

    > > Pbind(\degree,Pseq([Pshuf(#[-7,-3,0,2,4,7],4)+[0,4],
    > >                     Pseq( [0,1,2,3,4,5,6,7])+[0,2]],inf),
    > >       \dur,0.15).play

> p_08 =
>   let d = pseqr (\e -> [pshuf e [-7,-3,0,2,4,7] 4 + pmce2 0 4
>                        ,pseq [0,1,2,3,4,5,6,7] 1 + pmce2 0 2]) inf
>   in pbind [(K_degree,d),(K_dur,0.15)]

Modal transposition is at 'K_mtranspose':

> p_09 =
>   let d e = pseq [pshuf e [-7,-3,0,2,4,7] 4
>                  ,pseq [0,1,2,3,4,5,6,7] 1] 1
>       f t e = pbind [(K_dur,0.15)
>                     ,(K_mtranspose,t)
>                     ,(K_degree,d e)]
>       p = pzipWith f (pseq [0,1,2] inf) (pseries 0 1 inf)
>   in pjoin p

Chromatic transposition is at 'K_ctranspose':

> p_10 =
>   let d e = pseq [pshuf e [-7,-3,0,2,4,7] 4
>                  ,pseq [0,1,2,3,4,5,6,7] 1] 1
>       f t e = pbind [(K_dur,0.15)
>                     ,(K_ctranspose,t)
>                     ,(K_degree,d e)]
>       p = pzipWith f (pseq [0,3,-3] inf) (pseries 0 1 inf)
>   in pjoin p

The basic key for the duration model, 'Dur', is 'K_dur':

    > > Pbind(\dur,Pseq([Pgeom(0.05,1.1,24),
    > >                  Pgeom(0.5,0.909,24)],inf),
    > >       \midinote,Pseq(#[60,58],inf)).play

> p_11 = pbind [(K_dur,pseq [pgeom 0.05 1.1 24
>                           ,pgeom 0.5 0.909 24] inf)
>              ,(K_midinote,pseq [60,58] inf)]

The 'K_legato' field scales the sounding duration of events, but not
the logical time they occupy:

    > > Pbind(\dur,0.2,
    > >       \legato,Pseq([Pseries(0.05,0.05,40),
    > >                     Pseries(2.05,-0.05,40)],inf),
    > >       \midinote,Pseq(#[48,51,55,58,60,58,55,51],inf)).play

> p_12_a =
>   pbind [(K_dur,0.2)
>         ,(K_legato,pseq [pseries 0.05 0.05 40
>                         ,pseries 2.05 (-0.05) 40] inf)
>         ,(K_midinote,pseq [48,51,55,58,60,58,55,51] inf)]

    > > Pbind(\degree,Pseq([Pseries(-7,1,14),Pseries(7,-1,14)],inf),
    > >       \dur,0.25,
    > >       \legato,Pkey(\degree).linexp(-7,7,2.0,0.05)).play

> p_12_b =
>   let d = pseq [pseries (-7) 1 14,pseries 7 (-1) 14] inf
>       l = fmap (linexp_hs (-7,7) (2,0.05)) d
>   in pbind [(K_degree,d)
>            ,(K_dur,0.25)
>            ,(K_legato,l)]

The amplitude can be set as a linear value at key 'K_amp', or in
decibels below zero at key 'K_db', where zero is linear amplitude of
@1@:

> p_13 = pbind [(K_dur,0.2)
>              ,(K_degree,prand 'α' [0,1,5,7] inf)
>              ,(K_db,prand 'β' [-96,-48,-24,-12,-6] inf)]

As at /SC3/ a frequency value of /NaN/ indicates a rest.  There is a
constant value `nan` that can be used for this purpose a frequency
model input (not at a @rest@ key).

> p_14_a =
>   pbind [(K_dur,pseq [0.1,0.7] inf)
>         ,(K_legato,0.2)
>         ,(K_degree,pseq [0,2,return nan] inf)]

Alternately a non-zero `K_rest` key can be used.

> p_14_b =
>   pbind [(K_dur,0.25)
>         ,(K_amp,pseq [0.05,0.2] inf)
>         ,(K_rest,pseq [0,0,1] inf)]

A finite binding stops the `Event` pattern.

    > > Pbind(\freq,Prand([300,500,231.2,399.2],inf),
    > >       \dur,Pseq([0.1,0.2],3)).play;

> p_15_a =
>   pbind [(K_freq,prand 'α' [300,500,231.2,399.2] inf)
>         ,(K_dur,pseq [0.1,0.2] 3)]

    > > Pbind(\freq,Prand([300,500,231.2,399.2],inf),
    > >       \dur,Prand([0.1,0.3],inf)).play

All infinite inputs:

> p_15_b =
>   pbind [(K_freq,prand 'α' [300,500,231.2,399.2] inf)
>         ,(K_dur,prand 'β' [0.1,0.3] inf)]

Implicit /field/ patterns is this context are infinite.

> p_16 = pbind [(K_freq,prand 'α' [1,1.2,2,2.5,3,4] inf * 200)
>              ,(K_dur,0.1)]

The K_instr field sets the UGen graph to use.

> s_17 =
>   let freq = control KR "freq" 440
>       amp = control KR "amp" 0.1
>       nharms = control KR "nharms" 10
>       pan = control KR "pan" 0
>       gate = control KR "gate" 1
>       s = blip AR freq nharms * amp
>       e = linen gate 0.01 0.6 0.4 RemoveSynth
>       o = offsetOut 0 (pan2 s pan e)
>   in synthdef "s17" o

> p_17 = pbind [(K_instr,psynth s_17)
>              ,(K_freq,prand 'α' [1,1.2,2,2.5,3,4] inf * 200)
>              ,(K_dur,0.1)]

> p_18 = pbind [(K_instr,psynth s_17)
>              ,(K_param "nharms",pseq [4,10,40] inf)
>              ,(K_dur,pseq [1,1,2,1] inf / 10)
>              ,(K_freq,pn (pseries 1 1 16 * 50) 4)
>              ,(K_sustain,pseq [1/10,0.5,1,2] inf)]

> s_19 =
>   let freq = control KR "freq" 1000
>       gate = control KR "gate" 1
>       pan = control KR "pan" 0
>       cut = control KR "cut" 4000
>       res = control KR "res" 0.8
>       amp = control KR "amp" 1
>       s = rlpf (pulse AR freq 0.05) cut res
>       d = envLinen 0.01 1 0.3 1
>       e = envGen KR gate amp 0 1 RemoveSynth d
>       o = out 0 (pan2 s pan e)
>   in synthdef "s_19" o

    > > Pbind(\instrument,\s_19,
    > >       \dur,Pseq([0.25,0.5,0.25],4),
    > >       \root,-24,
    > >       \degree,Pseq([0,3,5,7,9,11,5,1],inf),
    > >       \pan,Pfunc({1.0.rand2}),
    > >       \cut,Pxrand([1000,500,2000,300],inf),
    > >       \rez,Pfunc({0.7.rand +0.3}),
    > >       \amp,0.2).play

> p_19 = pbind [(K_instr,psynth s_19)
>              ,(K_dur,pseq [0.25,0.5,0.25] 4)
>              ,(K_root,-24)
>              ,(K_degree,pseq [0,3,5,7,9,11,5,1] inf)
>              ,(K_param "pan",pwhite 'α' (-1.0) 1.0 inf)
>              ,(K_param "cut",pxrand 'β' [1000,500,2000,300] inf)
>              ,(K_param "res",pwhite 'γ' 0.3 1.0 inf)
>              ,(K_amp,0.2)]

    > > Pseq([Pbind(\instrument,\acid,
    > >             \dur,Pseq([0.25,0.5,0.25],4),
    > >             \root,-24,
    > >             \degree,Pseq([0,3,5,7,9,11,5,1],inf),
    > >             \pan,Pfunc({1.0.rand2}),
    > >            \cut,Pxrand([1000,500,2000,300],inf),
    > >            \rez,Pfunc({0.7.rand + 0.3}),
    > >            \amp,0.2),
    > >      Pbind(\instrument,\acid,
    > >            \dur,Pseq([0.25],6),
    > >            \root,-24,
    > >            \degree,Pseq([18,17,11,9],inf),
    > >            \pan,Pfunc({1.0.rand2}),
    > >            \cut,1500,
    > >            \rez,Pfunc({0.7.rand + 0.3}),
    > >            \amp,0.16)],inf).play

> p_20 = pseq [pbind [(K_instr,psynth s_19)
>                    ,(K_dur,pseq [0.25,0.5,0.25] 4)
>                    ,(K_root,-24)
>                    ,(K_degree,pseq [0,3,5,7,9,11,5,1] inf)
>                    ,(K_param "pan",pwhite 'α' (-1.0) 1.0 inf)
>                    ,(K_param "cut",pxrand 'β' [1000,500,2000,300] inf)
>                    ,(K_param "res",pwhite 'γ' 0.3 1.0 inf)
>                    ,(K_amp,0.2)]
>             ,pbind [(K_instr,psynth s_19)
>                    ,(K_dur,pn 0.25 6)
>                    ,(K_root,-24)
>                    ,(K_degree,pser [18,17,11,9] inf)
>                    ,(K_param "pan",pwhite 'δ' (-1.0) 1.0 inf)
>                    ,(K_param "cut",1500)
>                    ,(K_param "res",pwhite 'ε' 0.3 1.0 inf)
>                    ,(K_amp,0.16)]] inf

    > >Pbind(\instrument, \acid,
    > >      \dur, Pseq([0.25,0.5,0.25], inf),
    > >      \root, [-24,-17],
    > >      \degree, Pseq([0,3,5,7,9,11,5,1], inf),
    > >      \pan, Pfunc({1.0.rand2}),
    > >      \cut, Pxrand([1000,500,2000,300], inf),
    > >      \rez, Pfunc({0.7.rand +0.3}),
    > >      \amp, 0.2).play;

> p_21 = pbind [(K_instr,psynth s_19)
>              ,(K_dur,pseq [0.25,0.5,0.25] inf)
>              ,(K_root,pmce2 (-24) (-17))
>              ,(K_degree,pseq [0,3,5,7,9,11,5,1] inf)
>              ,(K_param "pan",pwhite 'α' (-1.0) 1.0 inf)
>              ,(K_param "cut",pxrand 'β' [1000,500,2000,300] inf)
>              ,(K_param "res",pwhite 'γ' 0.3 1.0 inf)
>              ,(K_amp,0.2)]

A persistent synthesis node with /freq/ and /amp/ controls.

> s_22 =
>   let freq = control KR "freq" 440
>       amp = control KR "amp" 0.6
>       n = pinkNoise 'α' AR * amp
>       o = out 0 (pan2 (moogFF n freq 2 0) 0 1)
>   in synthdef "s_22" o

A pattern to set /freq/ and /amp/ controls at the most recently
instantiated synthesis node, ie. s_22 above.

> p_22 = pbind [(K_type,prepeat (F_String "n_set"))
>              ,(K_id,(-1))
>              ,(K_freq,pwhite 'α' 100 1000 inf)
>              ,(K_dur,0.2)
>              ,(K_amp,toP [1,0.99 .. 0.1])]

> s_23 =
>   let k = control KR
>       f = k "freq" 80
>       a = k "amp" 0.01
>       p = k "pan" 0
>       g = k "gate" 1
>       env = decay2 g 0.05 8 * 0.0003
>       syn = rlpf (lfPulse AR f 0 (sinOsc KR 0.12 (mce2 0 (pi/2)) * 0.48 + 0.5))
>             (f * (sinOsc KR 0.21 0 * 18 + 20))
>             0.07
>       syn_env = syn * env
>       kil = detectSilence (mceChannel 0 syn_env) 0.1 0.2 RemoveSynth
>       o = mrg2 (out (k "out" 0) (a * mix (panAz 4 syn_env (mce2 p (p + 1)) 1 2 0.5))) kil
>   in synthdef "s_23" o

> p_23_a =
>   ppar [pbind [(K_degree,pseq [0,1,2,4,6,3,4,8] inf)
>               ,(K_dur,0.5)
>               ,(K_octave,3)
>               ,(K_instr,psynth s_23)]
>        ,pbind [(K_degree,pseq [0,1,2,4,6,3,4,8] inf)
>               ,(K_dur,0.5)
>               ,(K_octave,pmce2 2 1)
>               ,(K_param "pan",pwhite 'a' (-1) 1 inf)
>               ,(K_instr,psynth s_23)]]

> p_24 = pmono [(K_instr,pinstr' (Instr_Ref "default" False))
>              ,(K_id,100)
>              ,(K_degree,pxrand 'α' [0,2,4,5,7,9,11] inf)
>              ,(K_amp,pwrand 'β' [0.05,0.2] [0.7,0.3] inf)
>              ,(K_dur,0.25)]

> p_25 =
>   let n = 0.15
>   in pbind [(K_dur,prepeat n)
>            ,(K_fwd',toP (cycle [0,0,n,0,n,n,0,n,0,0,n*4]))
>            ,(K_legato,0.2)
>            ,(K_octave,prand 'α' [4,5,5,6] inf)
>            ,(K_degree,pxrand 'β' [0,1,5,7] inf)]

> p_26 =
>   let p = pbind [(K_dur,0.2),(K_midinote,pseq [62,65,69,72] inf)]
>       q = pbind [(K_dur,0.4),(K_midinote,pseq [50,45] inf)]
>       r = pbind [(K_dur,0.6),(K_midinote,pseq [76,79,81] inf)]
>   in ppar [p,q,r]

Multiple nested `ppar` patterns.

> p_27 =
>   let a u = pbind [(K_dur,0.2),(K_param "pan",0.5),(K_midinote,pseq u 1)]
>       b l = pbind [(K_dur,0.4),(K_param "pan",-0.5),(K_midinote,pseq l 1)]
>       f u l = ppar [a u,b l]
>       h = pbind [(K_dur,prand 'α' [0.2,0.4,0.6] inf)
>                 ,(K_midinote,prand 'β' [72,74,76,77,79,81] inf)
>                 ,(K_db,-26)
>                 ,(K_legato,1.1)]
>       m = pseq [pbind [(K_dur,3.2),(K_freq,return nan)]
>                ,prand 'γ' [f [60,64,67,64] [48,43]
>                           ,f [62,65,69,65] [50,45]
>                           ,f [64,67,71,67] [52,47]] 12] inf
>  in ppar [h,m]

> p_28 =
>   let d = pseq [pshuf 'α' [-7,-3,0,2,4,7] 2
>                ,pseq [0,1,2,3,4,5,6,7] 1] 1
>       p = pbind [(K_dur,0.15),(K_degree,d)]
>   in pseq [p,pstretch 0.5 p,pstretch 2 p] inf

> p_29 =
>   let f d p n = pbind [(K_dur,d),(K_param "pan",p),(K_midinote,n)]
>       a = f 0.2 (-1) (pseries 60 1 15)
>       b = f 0.15 0 (pseries 58 2 15)
>       c = f 0.1 1 (pseries 46 3 15)
>   in ptpar [(0,a),(1,b),(2,c)]

> p_30 =
>   let d = pseq [pgeom 0.05 1.1 24,pgeom 0.5 0.909 24] 2
>       f n a p = pbind [(K_dur,d)
>                       ,(K_db,a)
>                       ,(K_param "pan",p)
>                       ,(K_midinote,pseq [n,n-4] inf)]
>  in ptpar [(0,f 53 (-20) (-0.9))
>           ,(2,f 60 (-23) (-0.3))
>           ,(4,f 67 (-26) 0.3)
>           ,(6,f 74 (-29) 0.9)]

> p_31 = pbind [(K_instr,pinstr' defaultInstr)
>              ,(K_degree,toP [0,2,4,7])
>              ,(K_dur,0.25)]

> s_32 =
>   let f = control KR "freq" 440
>       g = control KR "gate" 1
>       a = control KR "amp" 0.1
>       d = envASR 0.01 1 1 (EnvNum (-4))
>       e = envGen KR g a 0 1 RemoveSynth d
>       o = out 0 (sinOsc AR f 0 * e)
>   in synthdef "s_32" o

> p_32 = pbind [(K_instr,psynth s_32)
>              ,(K_degree,toP [0,2,4,7])
>              ,(K_dur,0.25)]

> p_33 =
>   let si = return (F_Instr (Instr_Ref "s_32" True))
>       di = return (F_Instr (Instr_Ref "default" True))
>       i = pseq [si,si,di] inf
>   in pbind [(K_instr,i),(K_degree,pseq [0,2,4,7] inf),(K_dur,0.25)]

> p_34 =
>   let a = pseq [65,69,74] inf
>       b = pseq [60,64,67,72,76] inf
>       c = pseq [pmce3 72 76 79,pmce2 a b] 1
>   in p_un_mce (pbind [(K_midinote,c)
>                      ,(K_param "pan",pmce2 (-1) 1)
>                      ,(K_dur,1 `pcons` prepeat 0.15)])

> p_35 =
>   p_un_mce
>   (pbind [(K_dur,pmce2 0.25 0.2525)
>          ,(K_legato,pmce2 0.25 2.5)
>          ,(K_freq,pmce2 (pseq [300,400,500] inf)
>                   (pseq [302,402,502,202] inf))
>          ,(K_param "pan",pmce2 (-0.5) 0.5)])

> p_36 =
>   let p = pbind [(K_dur,0.2),(K_midinote,pseq [62,65,69,72] inf)]
>       q = pbind [(K_dur,0.4),(K_midinote,pseq [50,45] inf)]
>   in pmerge p q

    > > Pbind(\degree,Pstutter(Pwhite(3,10,inf),Pwhite(-4,11,inf)),
    > >       \dur,Pclutch(Pwhite(0.1,0.4,inf),
    > >                    Pdiff(Pkey(\degree)).abs > 0),
    > >       \legato,0.3).play;

> p_37 =
>   let d = pstutter (pwhite 'α' 3 10 inf) (pwhitei 'β' (-4) 11 inf)
>       p = [(K_degree,d)
>           ,(K_dur,pclutch (pwhite 'γ' 0.1 0.4 inf)
>                   (pbool (abs (pdiff d) `sc3_gt` 0)))
>           ,(K_legato,0.3)]
>   in pbind p

    > > Pbind(\degree,Pseq([-7,Pwhite(0,11,inf)],1),
    > >       \dur,Pconst(4,Pwhite(1,4,inf) * 0.25)).play

> p_38 =
>   let p = [(K_degree,pcons (-7) (pwhitei 'α' 0 11 inf))
>           ,(K_dur,pconst 4 (pwhite 'β' 1 4 inf * 0.25) 0.001)]
>   in pbind p

    > > Pbind(\note,PdegreeToKey(Pseq([1,2,3,2,5,4,3,4,2,1],2),
    > >                          #[0,2,3,6,7,9],
    > >                          12),\dur,0.25).play

> p_39 =
>   let n = pdegreeToKey (pseq [1,2,3,2,5,4,3,4,2,1] 2)
>           (pure [0,2,3,6,7,9])
>           12
>   in pbind [(K_note,n),(K_dur,0.25)]

    > > s = #[[0,2,3,6,7,9],[0,1,5,6,7,9,11],[0,2,3]];
    > > d = [1,2,3,2,5,4,3,4,2,1];
    > > Pbind(\note,PdegreeToKey(Pseq(d,4),
    > >                          Pstutter(3,Prand(s,inf)),
    > >                          12),\dur,0.25).play;

> p_40 =
>   let s = map return [[0,2,3,6,7,9],[0,1,5,6,7,9,11],[0,2,3]]
>       d = [1,2,3,2,5,4,3,4,2,1]
>       k = pdegreeToKey (pseq d 4)
>           (pstutter 3 (prand 'α' s 14))
>           (pn 12 40)
>   in pbind [(K_note,k),(K_dur,0.25)]

pdurStutter, applied to duration:

    > > d = PdurStutter(Pseq(#[1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4],inf),
    > >                 Pseq(#[0.5,1,2,0.25,0.25],inf));
    > > Pbind(\freq,440,\dur,d).play

> p_41 =
>   let s = pseq [1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4] inf
>       d = pseq [0.5,1,2,0.25,0.25] inf
>    in pbind [(K_freq,440),(K_dur,pdurStutter s d)]

Applied to frequency.

> p_42 =
>   let s = pseq [1,1,1,1,1,2,2,2,2,2,3,3,3,3,4,4,0,4,4] inf
>       d = pseq [0,2,3,5,7,9,10] inf + 80
>   in pbind [(K_midinote,pdurStutter s d),(K_dur,0.15)]

    > > p = Pfuncn({exprand(0.1,0.3) + #[1,2,3,6,7].choose},inf);
    > > Pbind(\freq,p * 100 + 300,\dur,0.02).play

> p_43 =
>   let p = pfuncn 'α' (Gen.exprand 0.1 0.3) inf
>       q = pfuncn 'β' (Gen.choose [1,2,3,6,7]) inf
>   in pbind [(K_freq,(p + q) * 100 + 300),(K_dur,0.02)]

Of course in this case there is a pattern equivalent.

> p_44 =
>   let p = pexprand 'α' 0.1 0.3 inf + prand 'β' [1,2,3,6,7] inf
>   in pbind [(K_freq,p * 100 + 300),(K_dur,0.02)]

    > > Pbind(\degree,Pseries(-7,1,15),
    > >       \dur,Pgeom(0.5,0.89140193218427,15)).play;

> p_45 = pbind [(K_degree,pseries (-7) 1 15)
>              ,(K_dur,pgeom 0.5 0.89140193218427 15)]

    > > Pbind(\degree,Ppatlace([Pseries(0,1,8),Pseries(2,1,7)],inf),
    > >       \dur,0.25).play;

> p_46 =
>   pbind [(K_degree,ppatlace [pseries 0 1 8,pseries 2 1 7] inf)
>         ,(K_dur,0.125)]

Random frequencies:

    > > Pbind(\freq,Prand([300,500,231.2,399.2],inf),
    > >       \dur,0.1).play;

> p_47 = pbind [(K_freq,prand 'α' [300,500,231.2,399.2] inf)
>              ,(K_dur,0.1)]

Random frequencies and durations:

    > > Pbind(\freq, Prand([300,500,231.2,399.2],inf),
    > >       \dur,Prand([0.1,0.3],inf)).play;

> p_48 = pbind [(K_freq,prand 'α' [300,500,231.2,399.2] inf)
>              ,(K_dur,prand 'β' [0.1,0.3] inf)]

Random multipliers of a fundamental frequency:

    > > Pbind(\freq,Prand([1,1.2,2,2.5,3,4],inf) * 200,
    > >       \dur,0.1).play;

> p_49 = pbind [(K_freq,prand 'α' [1,1.2,2,2.5,3,4] inf * 200)
>              ,(K_dur,0.1)]

Nested sequences of pitches:

    > > Pbind(\midinote,Prand([Pseq(#[60,61,63,65,67,63]),
    > >                        Prand(#[72,73,75,77,79],6),
    > >                        Pshuf(#[48,53,55,58],2)],inf),
    > >       \dur,0.25).play

> p_50 =
>   let n = prand 'α' [pseq [60,61,63,65,67,63] 1
>                     ,prand 'β' [72,73,75,77,79] 6
>                     ,pshuf 'γ' [48,53,55,58] 2] inf
>   in pbind [(K_midinote,n),(K_dur,0.075)]

    > > Pbind(\degree,Pseries(4,1,inf).fold(-7,11),
    > >       \dur,Prorate(0.6,0.5)).play

> p_51 = pbind [(K_degree,pfold (pseries 4 1 inf) (-7) 11)
>              ,(K_dur,prorate (fmap Left 0.6) 0.25)]

As scale degrees.

    > > Pbind(\degree,Pseq(#[0,0,4,4,5,5,4],1),
    > >       \dur,Pseq(#[0.5,0.5,0.5,0.5,0.5,0.5,1],1)).play

> p_52 = pbind [(K_degree,pseq [0,0,4,4,5,5,4] 1)
>              ,(K_dur,pseq [0.5,0.5,0.5,0.5,0.5,0.5,1] 1)]

    > > Pseq(#[60,62,63,65,67,63],inf) + Pseq(#[0,0,0,0,-12],inf)

> p_53 =
>   let n = pseq [60,62,63,65,67,63] inf + pser [0,0,0,0,-12] 25
>   in pbind [(K_midinote,n),(K_dur,0.2)]

Pattern `b` pattern sequences `a` once normally, once transposed up a
fifth and once transposed up a fourth.

    > > a = Pseq(#[60,62,63,65,67,63]);
    > > b = Pseq([a,a + 7,a + 5],inf);
    > > Pbind(\midinote,b,\dur,0.3).play

> p_54 =
>   let a = pseq [60,62,63,65,67,63] 1
>       b = pseq [a,a + 7,a + 5] inf
>   in pbind [(K_midinote,b),(K_dur,0.13)]

    > > Pbind(\degree,Pslide((-6,-4 .. 12),8,3,1,0),
    > >       \dur,Pseq(#[0.1,0.1,0.2],inf),
    > >       \sustain,0.15).play

> p_55 = pbind [(K_degree,pslide [-6,-4 .. 12] 8 3 1 0 True)
>              ,(K_dur,pseq [0.05,0.05,0.1] inf)
>              ,(K_sustain,0.15)]

Stutter scale degree and duration with the same random sequence.

    > > Pbind(\n,Pwhite(3,10,inf),
    > >       \degree,Pstutter(Pkey(\n),Pwhite(-4,11,inf)),
    > >       \dur,Pstutter(Pkey(\n),Pwhite(0.05,0.4,inf)),
    > >       \legato,0.3).play

> p_56 =
>   let n = pwhite 'α' 3 10 inf
>       p = [(K_degree,pstutter n (pwhitei 'β' (-4) 11 inf))
>           ,(K_dur,pstutter n (pwhite 'γ' 0.05 0.4 inf))
>           ,(K_legato,0.3)]
>   in pbind p

The pattern below is alternately lower and higher noise.

> p_57 =
>   let l = pseq [0.0,9.0] inf
>       h = pseq [1.0,12.0] inf
>   in pbind [(K_freq,pwhite_rp 'α' l h * 20 + 800)
>            ,(K_dur,0.25)]

    > > Pbind(\degree,Pwrand((0..7),[4,1,3,1,3,2,1].normalizeSum,inf),
    > >       \dur,0.25).play;

> p_58 =
>   let w = SC3.normalizeSum [4,1,3,1,3,2,1]
>       d = pwrand 'α' (Collection.series 7 0 1) w inf
>   in pbind [(K_degree,d),(K_dur,0.25)]

> p_59 =
>   let p = prand' 'α' [pempty,toP [24,31,36,43,48,55]] inf
>       q = pflop [60,prand 'β' [63,65] inf
>                 ,67,prand 'γ' [70,72,74] inf]
>       r = psplitPlaces (pwhite 'δ' 3 9 inf) (toP [74,75,77,79,81])
>       n = pjoin (pseq1 [p,q,r] inf)
>   in pbind [(K_midinote,n),(K_dur,0.13)]

`pappend` is an alias for `<>`.

> p_60 =
>   let p = prand 'α' [0,1] 3
>       q = prand 'β' [5,7] 3
>   in pbind [(K_degree,pappend p q <> p),(K_dur,0.15)]

    > paudition p_01
    > paudition p_02
    > paudition p_03
    > paudition p_04
    > paudition p_05
    > paudition p_06
    > paudition p_07
    > paudition p_08
    > paudition p_09
    > paudition p_10
    > paudition p_11
    > paudition p_12
    > paudition p_13
    > paudition p_14_a
    > paudition p_14_b
    > paudition p_15
    > paudition p_16
    > paudition p_17
    > paudition p_18
    > paudition p_19
    > paudition p_20
    > paudition p_21
    > audition s_22
    > paudition p_22
    > paudition p_23_a
    > paudition p_23_b
    > paudition p_24
    > paudition p_25
    > paudition p_26
    > paudition p_27
    > paudition p_28
    > paudition p_29
    > paudition p_30
    > paudition p_31
    > paudition p_32
    > paudition p_33
    > paudition p_34
    > paudition p_35
    > paudition p_36
    > paudition p_37
    > paudition p_38
    > paudition p_39
    > paudition p_40
    > paudition p_41
    > paudition p_42
    > paudition p_43
    > paudition p_44
    > paudition p_45
    > paudition p_46
    > paudition p_47
    > paudition p_48
    > paudition p_49
    > paudition p_50
    > paudition p_51
    > paudition p_52
    > paudition p_53
    > paudition p_54
    > paudition p_55
    > paudition p_56
    > paudition p_57
    > paudition p_58
    > paudition p_59
    > paudition p_60
    > paudition p_61
