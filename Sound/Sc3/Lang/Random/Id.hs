-- | 'Id' variants of "Sound.Sc3.Lang.Random.Gen".
module Sound.Sc3.Lang.Random.Id where

import System.Random {- random -}

import qualified Sound.Sc3.Lang.Random.Gen as Gen {- hsc3-lang -}

-- | /e/ is an identifier for 'mkStdGen' (via 'fromEnum'). /f/ is a function at 'StdGen'.
id_rand_ :: Enum e => e -> (StdGen -> a) -> a
id_rand_ e f = f (mkStdGen (fromEnum e))

-- | 'fst' of 'id_rand_'
id_rand :: Enum e => e -> (StdGen -> (a, StdGen)) -> a
id_rand e = fst . id_rand_ e

{- | Randomly select element from a collection n times.

>>> nchoose 'α' 9 [3,5]
[3,3,5,3,5,5,5,5,3]
-}
nchoose :: Enum e => e -> Int -> [a] -> [a]
nchoose e n = id_rand e . Gen.nchoose n

-- | Random number in (0, n).
rand :: (Random a, Num a, Enum e) => e -> a -> a
rand e = id_rand e . Gen.rand

-- | Random number in (n, m).
rrand :: (Random a, Enum e) => e -> a -> a -> a
rrand e l = id_rand e . Gen.rrand l

{- | Sequence of k random numbers in (n, m).

>>> nrrand 'α' 12 1 9
[1,6,1,4,2,6,8,1,2,8,2,9]
-}
nrrand :: (Random a, Enum e) => e -> Int -> a -> a -> [a]
nrrand e n l = id_rand e . Gen.nrrand n l

{- | Sequence of randomly selected boolean values with indicated bias, in (0, 1).

>>> ncoin 'α' 9 0.5
[True,False,False,False,True,False,True,False,False]
-}
ncoin :: (Random a, Ord a, Fractional a, Enum e) => e -> Int -> a -> [Bool]
ncoin e n = id_rand e . Gen.ncoin n

{- | Scramble elements of a collection.

>>> scramble 'α' ['a' .. 'z']
"yrzsojlvfdxaibwueqgktmhnpc"
-}
scramble :: Enum e => e -> [t] -> [t]
scramble e = id_rand e . Gen.scramble

-- * Stream

{- | Infinite stream of random numbers in (0, n).

>>> take 24 (s_rand 'α' 9)
[8,1,7,1,9,0,7,5,1,3,0,5,0,1,7,1,5,2,5,1,6,7,5,7]
-}
s_rand :: (Random n, Num n, Enum e) => e -> n -> [n]
s_rand e = id_rand_ e . Gen.s_rand

{- | Infinite stream of random numbers in (-n, n).

>>> take 24 (s_rand2 'α' 9)
[8,8,5,0,1,7,-4,8,-6,6,7,-9,2,1,-8,8,1,-4,9,4,8,-2,4,6]
-}
s_rand2 :: (Random n, Num n, Enum e) => e -> n -> [n]
s_rand2 e = id_rand_ e . Gen.s_rand2

{- | Infinite stream of random booleans with indicated bias, in (0, 1).

>>> take 4 (s_coin 'α' 0.25)
[False,False,True,False]
-}
s_coin :: (Random n, Ord n, Fractional n, Enum e) => e -> n -> [Bool]
s_coin e = id_rand_ e . Gen.s_coin

{- | Infinite stream of random selections from a collection.

>>> take 24 (s_choose 'α' ['a' .. 'z'])
"yrxrojkqxfrdpqvalkbxrkfs"
-}
s_choose :: Enum e => e -> [t] -> [t]
s_choose e = id_rand_ e . Gen.s_choose

{- | Infinite stream of random weighted selections from a collection.

>>> take 24 (s_wchoose 'α' "abc" [1/2, 1/4, 1/4])
"ccabacccaacbbaaccacaaaab"
-}
s_wchoose :: (Random n, Fractional n, Ord n, Enum e) => e -> [t] -> [n] -> [t]
s_wchoose e l = id_rand_ e . Gen.s_wchoose l

{- | Infinite stream of random weighted selections from a collection, weights are normalized before processing.

>>> take 24 (s_wchoose_n 'α' "abc" [2, 1, 1])
"ccabacccaacbbaaccacaaaab"
-}
s_wchoose_n :: (Random n, Fractional n, Ord n, Enum e) => e -> [t] -> [n] -> [t]
s_wchoose_n e l = id_rand_ e . Gen.s_wchoose_n l
