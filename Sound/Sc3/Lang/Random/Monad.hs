-- | 'Rand' monad based @sclang@ random number functions.
module Sound.Sc3.Lang.Random.Monad where

import Control.Monad {- base -}
import System.Random {- random  -}

import qualified Control.Monad.Random as Random {- MonadRandom -}

import qualified Sound.Sc3.Common.Buffer as Buffer {- hsc3 -}

import qualified Sound.Sc3.Lang.Collection as Collection {- hsc3-lang -}
import qualified Sound.Sc3.Lang.Math as Math {- hsc3-lang -}

{- | '<' /n/ of 'rand' 1

>>> Random.evalRand (replicateM 8 (coin 0.5)) (mkStdGen 6)
[True,True,False,True,False,True,False,False]
-}
coin :: (RandomGen g, Random n, Ord n, Num n) => n -> Random.Rand g Bool
coin n = fmap (< n) (rand 1)

{- | @SimpleNumber.rand@ is 'Random.getRandomR' in (0,/n/).

>>> Random.evalRand (replicateM 2 (rand (10::Int))) (mkStdGen 6)
[8,3]

>>> Random.evalRand (rand (1::Double)) (mkStdGen 6) == 0.04174387664757784
True
-}
rand :: (RandomGen g, Random n, Num n) => n -> Random.Rand g n
rand n = Random.getRandomR (0, n)

{- | 'replicateM' of 'rand'

>>> Random.evalRand (nrand 3 10) (mkStdGen 6)
[8,3,3]
-}
nrand :: (RandomGen g, Random n, Num n) => Int -> n -> Random.Rand g [n]
nrand k = replicateM k . rand

{- | @SimpleNumber.rand2@ is 'Random.getRandomR' in (-/n/,/n/).

>>> Random.evalRand (replicateM 2 (rand2 10)) (mkStdGen 5)
[1,9]
-}
rand2 :: (RandomGen g, Random n, Num n) => n -> Random.Rand g n
rand2 n = Random.getRandomR (-n, n)

{- | 'replicateM' of 'rand2'

>>> Random.evalRand (nrand2 3 10) (mkStdGen 5)
[1,9,-2]
-}
nrand2 :: (RandomGen g, Random n, Num n) => Int -> n -> Random.Rand g [n]
nrand2 k = replicateM k . rand2

{- | @SimpleNumber.rrand@ is 'curry' 'Random.getRandomR'.

>>> Random.evalRand (replicateM 2 (rrand 3 9)) (mkStdGen 1)
[8,6]
-}
rrand :: (RandomGen g, Random n) => n -> n -> Random.Rand g n
rrand = curry Random.getRandomR

{- | 'replicateM' of 'rrand'

>>> Random.evalRand (nrrand 4 3 9) (mkStdGen 1)
[8,6,5,7]
-}
nrrand :: (RandomGen g, Random n) => Int -> n -> n -> Random.Rand g [n]
nrrand k n = replicateM k . rrand n

{- | @SequenceableCollection.choose@ selects an element at random.

>>> Random.evalRand (choose [3..9]) (mkStdGen 1)
8
-}
choose :: RandomGen g => [a] -> Random.Rand g a
choose l = fmap (l !!) (rand (length l - 1))

-- | 'Collection.windex_err' of 'rrand'
wchoose :: (RandomGen g, Fractional t, Ord t, Random t) => [a] -> [t] -> Random.Rand g a
wchoose l w = fmap ((l !!) . Collection.windex_err w) (rrand 0.0 1.0)

-- | Normalise weights variant of 'wchoose'
wchoose_n :: (RandomGen g, Fractional t, Ord t, Random t) => [a] -> [t] -> Random.Rand g a
wchoose_n l w = wchoose l (Buffer.normalizeSum w)

{- | 'replicateM' of 'choose'

>>> Random.evalRand (nchoose 4 [3..9]) (mkStdGen 1)
[8,6,5,7]
-}
nchoose :: (RandomGen g) => Int -> [a] -> Random.Rand g [a]
nchoose k = replicateM k . choose

{- | @SimpleNumber.exprand@ generates exponentially distributed random number in the given interval.

>>> let r = replicateM 3 (exprand 10 100 >>= return . floor)
>>> Random.evalRand r (mkStdGen 1)
[13,35,33]
-}
exprand :: (Floating n, Random n, RandomGen g) => n -> n -> Random.Rand g n
exprand l r = fmap (Math.exprange l r) (rrand 0.0 1.0)

{- | 'replicateM' of 'exprand'

>>> let r = nexprand 3 10 100 >>= return . map floor
>>> Random.evalRand r (mkStdGen 1)
[13,35,33]
-}
nexprand :: (Floating n, Random n, RandomGen g) => Int -> n -> n -> Random.Rand g [n]
nexprand k l = replicateM k . exprand l
