-- | Scsynth operations.
module Sound.Sc3.Lang.Random.Server where

import Sound.Osc {- hosc -}
import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Lang.Random.Io as Random.Io {- hsc3-lang -}

-- | Get current data from buffer, scramble it, and write it back.
buf_scramble :: (MonadIO m, DuplexOsc m) => Int -> m ()
buf_scramble n = do
  (_, sz, _, _) <- b_query1_unpack n
  b <- b_getn1_data n (0, sz - 1)
  r <- liftIO (Random.Io.scramble b)
  sendMessage (b_setn1 n 0 r)
