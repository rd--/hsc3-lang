-- | 'RandomGen' based @sclang@ random number functions.
module Sound.Sc3.Lang.Random.Gen where

import Data.Maybe {- base  -}
import System.Random {- random -}

import qualified Data.DList as DList {- dlist -}
import qualified System.Random.Shuffle as Shuffle {- random-shuffle -}

import qualified Sound.Sc3.Common.Buffer as Buffer {- hsc3 -}

import qualified Sound.Sc3.Lang.Collection as Collection {- hsc3-lang -}
import qualified Sound.Sc3.Lang.Math as Math {- hsc3-lang -}

-- | Function underlying both 'kvariant' and 'kvariant_dl'.
mk_kvariant :: r -> (t -> r -> r) -> (r -> s) -> Int -> (g -> (t, g)) -> g -> (s, g)
mk_kvariant k_nil k_join un_k k f =
  let go x i g = case i of
        0 -> (un_k x, g)
        _ ->
          let (y, g') = f g
          in go (k_join y x) (i - 1) g'
  in go k_nil k

{- | Construct variant of /f/ generating /k/ values.
Note that the result is the reverse of the sequence given by the generator.

>>> fst (kvariant 12 (rand 12) (mkStdGen 0))
[4,9,7,9,8,7,12,6,11,8,8,8]
-}
kvariant :: Int -> (g -> (a, g)) -> g -> ([a], g)
kvariant = mk_kvariant [] (:) id

{- | Variant of 'kvariant' that generates sequence in the same order as the generator.
There is perhaps a slight overhead from using a difference list.

>>> fst (kvariant_dl 12 (rand 12) (mkStdGen 0))
[8,8,8,11,6,12,7,8,9,7,9,4]
-}
kvariant_dl :: Int -> (g -> (a, g)) -> g -> ([a], g)
kvariant_dl = mk_kvariant DList.empty (flip DList.snoc) DList.toList

-- | @SimpleNumber.rand@ is 'randomR' in (0,/n/).
rand :: (RandomGen g, Random n, Num n) => n -> g -> (n, g)
rand n = randomR (0, n)

{- | Variant of 'rand' generating /k/ values.

>>> fst (nrand 10 (5::Int) (mkStdGen 246873))
[5,5,1,0,3,2,4,3,0,5]
-}
nrand :: (RandomGen g, Random n, Num n) => Int -> n -> g -> ([n], g)
nrand k = kvariant k . rand

-- | @SimpleNumber.rand2@ is 'randomR' in (-/n/,/n/).
rand2 :: (RandomGen g, Random n, Num n) => n -> g -> (n, g)
rand2 n = randomR (-n, n)

-- | Variant of 'rand2' generating /k/ values.
nrand2 :: (RandomGen g, Random a, Num a) => Int -> a -> g -> ([a], g)
nrand2 k = kvariant k . rand2

-- | @SimpleNumber.rrand@ is 'curry' 'randomR'.
rrand :: (Random n, RandomGen g) => n -> n -> g -> (n, g)
rrand = curry randomR

-- | Variant of 'rrand' generating /k/ values.
nrrand :: (RandomGen g, Random a) => Int -> a -> a -> g -> ([a], g)
nrrand k l = kvariant k . rrand l

-- | @SequenceableCollection.choose@ selects an element at random.
choose :: RandomGen g => [a] -> g -> (a, g)
choose l g =
  let (i, g') = randomR (0, length l - 1) g
  in (l !! i, g')

-- | Variant of 'choose' generating /k/ values.
nchoose :: RandomGen g => Int -> [a] -> g -> ([a], g)
nchoose k = kvariant k . choose

-- | @SimpleNumber.exprand@ generates exponentially distributed random number in the given interval.
exprand :: (Floating n, Random n, RandomGen g) => n -> n -> g -> (n, g)
exprand l r g =
  let (n, g') = rrand 0.0 1.0 g
  in (Math.exprange l r n, g')

-- | Variant of 'exprand' generating /k/ values.
nexprand :: (Floating n, Random n, RandomGen g) => Int -> n -> n -> g -> ([n], g)
nexprand k l = kvariant k . exprand l

-- | NoiseUGens.cpp
linrand :: (Floating n, Random n, RandomGen g, Ord n) => n -> n -> n -> g -> (n, g)
linrand l r n g0 =
  let (a, g1) = rrand l r g0
      (b, g2) = rrand l r g1
  in (if n <= 0 then min a b * (r - l) + l else max a b * (r - l) + l, g2)

-- | @SimpleNumber.coin@ is 'True' at given probability, which is in range (0,1).
coin :: (RandomGen g, Random a, Ord a, Fractional a) => a -> g -> (Bool, g)
coin n g =
  let (i, g') = randomR (0.0, 1.0) g
  in (i < n, g')

{- | Variant of 'coin' generating /k/ values.

>>> fst (ncoin 5 0.5 (mkStdGen 0))
[False,False,True,True,True]
-}
ncoin :: (RandomGen g, Random a, Ord a, Fractional a) => Int -> a -> g -> ([Bool], g)
ncoin k = kvariant k . coin

{- | @List.scramble@ shuffles the elements.

> kvariant 10 (scramble "abcdefg") (mkStdGen 18764219)
-}
scramble :: RandomGen g => [t] -> g -> ([t], g)
scramble k g =
  let (_, g') = genWord32 g
  in (Shuffle.shuffle' k (length k) g, g')

{- | @SequenceableCollection.wchoose@ selects an element from a list given a list of weights which sum to @1@.

> kvariant 10 (wchoose "abcd" (Buffer.normalizeSum [8,4,2,1])) (mkStdGen 0)
-}
wchoose :: (RandomGen g, Random a, Ord a, Fractional a) => [b] -> [a] -> g -> (b, g)
wchoose l w g =
  let (i, g') = randomR (0.0, 1.0) g
      n = fromMaybe (error "wchoose: windex") (Collection.windex w i)
  in (l !! n, g')

{- | Variant that applies 'Buffer.normalizeSum' to weights.

>>> fst (kvariant 10 (wchoose_n "abcd" [8,4,2,1]) (mkStdGen 0))
"ccaacbbaaa"
-}
wchoose_n :: (Fractional a, Ord a, RandomGen g, Random a) => [b] -> [a] -> g -> (b, g)
wchoose_n l w = wchoose l (Buffer.normalizeSum w)

-- | 'kvariant' of 'wchoose_n'.
nwchoose_n :: (Fractional a, Ord a, RandomGen g, Random a) => Int -> [b] -> [a] -> g -> ([b], g)
nwchoose_n n l = kvariant n . wchoose_n l

-- * Stream

{- | State modifying variant of 'iterate', lifts random generator functions to infinte lists.

>>> take 12 (r_iterate (rand 12) (mkStdGen 0))
[8,8,8,11,6,12,7,8,9,7,9,4]
-}
r_iterate :: (t -> (a, t)) -> t -> [a]
r_iterate f g0 = let (r, g1) = f g0 in r : r_iterate f g1

{- | 'r_iterate' of 'rand'.

>>> take 10 (s_rand 100 (mkStdGen 246873))
[29,32,30,92,26,99,49,29,53,52]
-}
s_rand :: (RandomGen g, Random n, Num n) => n -> g -> [n]
s_rand = r_iterate . rand

{- | 'r_iterate' of 'rand2'.

>>> take 10 (s_rand2 100 (mkStdGen 246873))
[-71,-68,23,-70,10,18,54,-51,-71,81]
-}
s_rand2 :: (RandomGen g, Random n, Num n) => n -> g -> [n]
s_rand2 = r_iterate . rand2

{- | 'r_iterate' of 'rrand'.

>>> take 10 (s_rrand 100 200 (mkStdGen 246873))
[129,132,130,192,126,199,149,129,153,152]
-}
s_rrand :: (RandomGen g, Random n) => n -> n -> g -> [n]
s_rrand lhs = r_iterate . rrand lhs

-- | 'r_iterate' of 'coin'.
s_coin :: (Random n, Ord n, Fractional n, RandomGen g) => n -> g -> [Bool]
s_coin = r_iterate . coin

-- | 'r_iterate' of 'choose'.
s_choose :: RandomGen g => [a] -> g -> [a]
s_choose = r_iterate . choose

{- | 'r_iterate' of 'wchoose'.

>>> take 24 (s_wchoose "abc" [2/3, 1/6, 1/6] (mkStdGen 17982))
"abcaaccbcacacaaaacbbaaaa"
-}
s_wchoose :: (RandomGen g, Random a, Ord a, Fractional a) => [b] -> [a] -> g -> [b]
s_wchoose l = r_iterate . wchoose l

{- | 'r_iterate' of 'wchoose_n'.

>>> take 24 (s_wchoose_n "abc" [4, 1, 1] (mkStdGen 17982))
"abcaaccbcacacaaaacbbaaaa"
-}
s_wchoose_n :: (RandomGen g, Random a, Ord a, Fractional a) => [b] -> [a] -> g -> [b]
s_wchoose_n l = r_iterate . wchoose_n l
