-- | 'getStdRandom' variants of "Sound.Sc3.Lang.Random.Gen".
module Sound.Sc3.Lang.Random.Io where

import Control.Monad.IO.Class {- transformers -}
import System.Random {- random -}

import qualified Sound.Sc3.Lang.Random.Gen as Gen {- hsc3-lang -}

-- | 'liftIO' of 'randomRIO'.
randomM :: (Random a, MonadIO m) => (a, a) -> m a
randomM = liftIO . randomRIO

-- | 'liftIO' of 'getStdRandom'.
randomG :: MonadIO m => (StdGen -> (a, StdGen)) -> m a
randomG = liftIO . getStdRandom

{- | 'Gen.rand'.

> import Control.Monad
> replicateM 10 (rand 100) -- [27,16,70,89,20,46,90,98,73,35]
-}
rand :: (MonadIO m, Random n, Num n) => n -> m n
rand = randomG . Gen.rand

-- | 'Gen.nrand'.
nrand :: (Random a, Num a) => Int -> a -> IO [a]
nrand n = randomG . Gen.nrand n

-- | 'Gen.rand2'.
rand2 :: (MonadIO m, Random n, Num n) => n -> m n
rand2 = randomG . Gen.rand2

-- | 'Gen.nrand2'.
nrand2 :: (Random a, Num a) => Int -> a -> IO [a]
nrand2 n = randomG . Gen.nrand2 n

-- | 'Gen.rrand'.
rrand :: (MonadIO m, Random n) => n -> n -> m n
rrand l = randomG . Gen.rrand l

{- | 'Gen.nrrand'.

> nrrand 9 (-9) 9 -- [-4,2,8,1,-5,7,7,9,4]
-}
nrrand :: (MonadIO m, Random a) => Int -> a -> a -> m [a]
nrrand n l = randomG . Gen.nrrand n l

-- | 'Gen.choose'.
choose :: MonadIO m => [a] -> m a
choose = randomG . Gen.choose

-- | 'Gen.nchoose'.
nchoose :: MonadIO m => Int -> [a] -> m [a]
nchoose n = randomG . Gen.nchoose n

-- | 'Gen.exprand'.
exprand :: (MonadIO m, Floating n, Random n) => n -> n -> m n
exprand l = randomG . Gen.exprand l

-- | 'Gen.exprand'.
nexprand :: (MonadIO m, Floating n, Random n) => Int -> n -> n -> m [n]
nexprand n l = randomG . Gen.nexprand n l

-- | 'Gen.coin'.
coin :: (MonadIO m, Random n, Fractional n, Ord n) => n -> m Bool
coin = randomG . Gen.coin

-- | 'Gen.coin'.
ncoin :: (MonadIO m, Random n, Fractional n, Ord n) => Int -> n -> m [Bool]
ncoin n = randomG . Gen.ncoin n

-- | 'Gen.scramble'.
scramble :: MonadIO m => [t] -> m [t]
scramble = randomG . Gen.scramble

-- | 'Gen.wchoose'.
wchoose :: (MonadIO m, Random a, Ord a, Fractional a) => [b] -> [a] -> m b
wchoose l = randomG . Gen.wchoose l

-- | 'Gen.wchoose_n'.
wchoose_n :: (MonadIO m, Random a, Ord a, Fractional a) => [b] -> [a] -> m b
wchoose_n l = randomG . Gen.wchoose_n l

-- | 'Gen.nwchoose_n'.
nwchoose_n :: (MonadIO m, Random a, Ord a, Fractional a) => Int -> [b] -> [a] -> m [b]
nwchoose_n n l = randomG . Gen.nwchoose_n n l
