-- | Composite module.
module Sound.Sc3.Lang.Pattern (module P) where

import Sound.Sc3.Lang.Control.Duration as P
import Sound.Sc3.Lang.Control.Event as P
import Sound.Sc3.Lang.Control.Instrument as P
import Sound.Sc3.Lang.Control.OverlapTexture as P
import Sound.Sc3.Lang.Control.Pitch as P

import Sound.Sc3.Lang.Pattern.P as P
import Sound.Sc3.Lang.Pattern.P.Event as P
