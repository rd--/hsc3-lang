-- | Infinte list @Sc3@ pattern functions.
module Sound.Sc3.Lang.Pattern.Stream where

import Data.List {- base -}
import Data.Maybe {- base -}
import System.Random {- random -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Lang.Math as Math {- hsc3-lang -}
import qualified Sound.Sc3.Lang.Random.Gen as Gen {- hsc3-lang -}

{- | Remove successive duplicates.

>>> remove_successive_duplicates [1,1,2,2,3,3]
[1,2,3]

>>> remove_successive_duplicates [1,2,3,1,2,3]
[1,2,3,1,2,3]
-}
remove_successive_duplicates :: Eq a => [a] -> [a]
remove_successive_duplicates =
  let f (p, _) i = (Just i, if Just i == p then Nothing else Just i)
  in mapMaybe snd . scanl f (Nothing, Nothing)

-- | True if /a/ is initially equal to /b/.
initially_equal :: Eq a => [a] -> [a] -> Bool
initially_equal = flip isPrefixOf

-- | Alias for 'initially_equal'
ieq :: Eq a => [a] -> [a] -> Bool
ieq = initially_equal

{- | Take elements from /l/ until all elements in /s/ have been seen.
If /s/ contains duplicate elements these must be seen multiple times.

>>> take_until_forms_set "abc" "a random sentence beginning"
"a random sentence b"
-}
take_until_forms_set :: Eq a => [a] -> [a] -> [a]
take_until_forms_set s l =
  if null s
    then []
    else case l of
      [] -> []
      e : l' -> e : take_until_forms_set (delete e s) l'

-- | Underlying 'brown'.
brown_ :: (RandomGen g, Random n, Num n, Ord n) => (n, n, n) -> (n, g) -> (n, g)
brown_ (l, r, s) (n, g) =
  let (i, g') = randomR (-s, s) g
  in (Sc3.foldToRange l r (n + i), g')

brown' :: (RandomGen g, Num t, Ord t, Random t) => (t, g) -> [t] -> [t] -> [t] -> [t]
brown' i l_ r_ s_ =
  let recur (n, g) z =
        case z of
          [] -> []
          (l, r, s) : z' ->
            let (n', g') = brown_ (l, r, s) (n, g)
            in n' : recur (n', g') z'
  in recur i (zip3 l_ r_ s_)

{- | Brown noise with list inputs and random intial value.

>>> take 5 (brown 'α' (repeat 1) (repeat 700) (cycle [1,20]))
[665,674,674,664,665]
-}
brown :: (Enum e, Random n, Num n, Ord n) => e -> [n] -> [n] -> [n] -> [n]
brown e l_ r_ = brown' (randomR (head l_, head r_) (mkStdGen (fromEnum e))) l_ r_

-- | 'Math.exprange' of 'white'
exprand :: (Enum e, Random a, Floating a) => e -> a -> a -> [a]
exprand e l r = fmap (Math.exprange l r) (white e 0 1)

{- | Geometric series.

>>> take 7 (geom 3 6)
[3,18,108,648,3888,23328,139968]
-}
geom :: Num a => a -> a -> [a]
geom i s = iterate (* s) i

{- | 'concat' of 'transpose'.

>>> lace [[0],[1,2],[3,4,5]] `ieq` [0,1,3,0,2,4,0,1,5]
True

>>> lace [[1],[2,5],[3,6]] `ieq` [1,2,3,1,5,6]
True

>>> lace [[1],[2,5],[3,6..]] `ieq` [1,2,3,1,5,6,1,2,9,1,5,12]
True
-}
lace :: [[a]] -> [a]
lace = concat . transpose . map cycle

{- | Random elements from list.

>>> take_until_forms_set "string" (rand 'α' "string")
"stgttrrsgtrisgsirttrggrggtggsgttigttirisiggsgggtrign"
-}
rand :: Enum e => e -> [a] -> [a]
rand e a =
  let k = length a - 1
  in map (a !!) (white e 0 k)

{- | List section with /wrapped/ indices.

>>> segment [0..4] 5 (3,5)
[3,4,0]
-}
segment :: [a] -> Int -> (Int, Int) -> [a]
segment a k (l, r) =
  let i = map (Sc3.generic_wrap (0, k - 1)) [l .. r]
  in map (a !!) i

{- | Sc3 slide.

>>> slide [1,2,3,4] 4 1 0 True `ieq` [[1,2,3,4],[2,3,4,1],[3,4,1,2],[4,1,2,3]]
True

>>> slide [1,2,3,4,5] 3 (-1) 0 True `ieq` [[1,2,3],[5,1,2],[4,5,1],[3,4,5],[2,3,4]]
True
-}
slide :: [a] -> Int -> Int -> Int -> Bool -> [[a]]
slide a j s i wr =
  let k = length a
      l = enumFromThen i (i + s)
      r = map (+ (j - 1)) l
  in if wr
      then zipWith (curry (segment a k)) l r
      else error "slide: non-wrap variant not implemented"

-- | 'concat' of 'slide'.
slide_c :: [a] -> Int -> Int -> Int -> Bool -> [a]
slide_c a j s u = concat . slide a j s u

{- | White noise.

>>> take_until_forms_set [1 .. 5] (white 'α' 1 5)
[1,2,2,2,3,3,1,2,3,4,1,1,4,3,2,2,3,3,2,1,2,2,4,2,2,4,3,4,1,4,1,2,3,4,5]
-}
white :: (Random n, Enum e) => e -> n -> n -> [n]
white e l r = randomRs (l, r) (mkStdGen (fromEnum e))

-- | Weighted selection of elements from a list.
wrand_generic :: (Enum e, Fractional n, Ord n, Random n) => e -> [a] -> [n] -> [a]
wrand_generic e a w =
  let f g =
        let (r, g') = Gen.wchoose a w g
        in r : f g'
  in if length a /= length w
      then error "wrand_generic: a/w must be of equal length"
      else f (mkStdGen (fromEnum e))

{- | Type restricted variant.

>>> let w = Sc3.normalizeSum [1 .. 5]
>>> let r = wrand 'ζ' "wrand" w
>>> take_until_forms_set "wrand" r
"dwrrnnnnrrnddnnwdwddndwdrdndddra"
-}
wrand :: Enum e => e -> [a] -> [Double] -> [a]
wrand = wrand_generic

{- | Select elements from /l/ in random sequence, but do not immediately repeat an element.

>>> take_until_forms_set "string" (xrand 'α' "string")
"stgtrsgtrisgsirtrgrgtgsgtigtirisigsgtrign"
-}
xrand :: Enum e => e -> [a] -> [a]
xrand e a =
  let g = mkStdGen (fromEnum e)
      k = length a - 1
      r = remove_successive_duplicates (randomRs (0, k) g)
  in map (a !!) r

{- | Iterate function /f/ with initial state of /st0/.

>>> take 5 (fiter 0 (\x -> (-x,x + 1)))
[0,-1,-2,-3,-4]
-}
fiter :: st -> (st -> (r, st)) -> [r]
fiter st0 f = let (r, st1) = f st0 in r : fiter st1 f
