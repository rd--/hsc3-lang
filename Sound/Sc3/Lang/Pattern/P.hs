{- | Composite of Pattern.P modules.
See <http://rd.slavepianos.org/?t=hsc3-texts> for tutorial.
-}
module Sound.Sc3.Lang.Pattern.P (module P) where

import Sound.Sc3.Lang.Pattern.P.Base as P
import Sound.Sc3.Lang.Pattern.P.Core as P
import Sound.Sc3.Lang.Pattern.P.Sc3 as P
