{- | @sclang@ value pattern functions.

Sc3 /value/ patterns: `pbrown` (Pbrown), `pclutch` (Pclutch),
`pcollect` (Pcollect), `pconst` (Pconst), `pdegreeToKey`
(PdegreeToKey), `pdiff` (Pdiff), `pdrop` (Pdrop), `pdurStutter`
(PdurStutter), `pexprand` (Pexprand), `pfinval` (Pfinval), `pfuncn`
(Pfuncn), `pgeom` (Pgeom), `pif` (Pif), `place` (Place), `pn` (Pn),
`ppatlace` (Ppatlace), `prand` (Prand), `preject` (Preject),
`prorate` (Prorate), `pselect` (Pselect), `pseq` (Pseq), `pser`
(Pser), `pseries` (Pseries), `pshuf` (Pshuf), `pslide` (Pslide),
`pstutter` (Pstutter), `pswitch1` (Pswitch1), `pswitch` (Pswitch),
`ptuple` (Ptuple), `pwhite` (Pwhite), `pwrand` (Pwrand), `pwrap`
(Pwrap), `pxrand` (Pxrand).

Sc3 variant patterns: `pbrown`', `prand'`, `prorate'`, `pseq1`,
`pseqn`, `pser1`, `pseqr`, `pwhite'`, `pwhitei`.

Sc3 collection patterns: `pfold`
-}
module Sound.Sc3.Lang.Pattern.P.Sc3 where

import Control.Monad {- base -}
import Data.List {- base -}
import System.Random {- random -}

import Sound.Sc3 {- hsc3 -}

import Sound.Sc3.Lang.Core
import Sound.Sc3.Lang.Pattern.P.Base
import Sound.Sc3.Lang.Pattern.P.Core

import qualified Sound.Sc3.Lang.Collection as C
import qualified Sound.Sc3.Lang.Math as Math
import qualified Sound.Sc3.Lang.Pattern.List as List
import qualified Sound.Sc3.Lang.Pattern.Stream as Stream
import qualified Sound.Sc3.Lang.Random.Gen as Gen

-- * Sc3 Collection Patterns

{- | Variant of 'C.flop'.

>>> pflop' [toP [1,2],toP [3,4,5]] == toP [[1,3],[2,4],[1,5]]
True

>>> pflop' [toP [1,2],3] == toP [[1,3],[2,3]]
True

>>> ptake 3 (pflop' [pseq [1,2] 1,pseq [3,4] inf]) == toP [[1,3],[2,4],[1,3]]
True
-}
pflop' :: [P a] -> P [a]
pflop' l = toP (C.flop (map unP l))

{- | 'fmap' 'toP' of 'pflop''.

>>> C.flop [[1,2],[3,4,5]]
[[1,3],[2,4],[1,5]]

>>> pflop [toP [1,2],toP [3,4,5]] == toP (map toP [[1,3],[2,4],[1,5]])
True
-}
pflop :: [P a] -> P (P a)
pflop = fmap toP . pflop'

{- | Type specialised 'List.ffold'.

>>> pfold (toP [10,11,12,-6,-7,-8]) (-7) 11 == toP [10,11,10,-6,-7,-6]
True

> import Sound.Sc3.Lang.Pattern
> paudition (pbind [(K_degree,pfold (pseries 4 1 inf) (-7) 11),(K_dur,0.0625)])

The underlying primitive is the `List.ffold` function.

> let f = fmap (\n -> List.ffold n (-7) 11)
> paudition (pbind [(K_degree,f (pseries 4 1 inf)),(K_dur,0.0625)])
-}
pfold :: (RealFrac n) => P n -> n -> n -> P n
pfold = List.ffold

-- | Pattern variant of 'normalizeSum'.
pnormalizeSum :: Fractional n => P n -> P n
pnormalizeSum = liftP normalizeSum

-- * Sc3 Patterns

{- | Pbrown.  Lifted 'List.brown'.  Sc3 pattern to generate
psuedo-brownian motion.

> pbrown 'α' 0 9 1 5 == toP [4,4,5,4,3]
> paudition (pbind [(K_dur,0.065),(K_freq,pbrown 'α' 440 880 20 inf)])
-}
pbrown :: (Enum e, Random n, Num n, Ord n) => e -> n -> n -> n -> Int -> P n
pbrown e l r s n = ptake n (toP (List.brown e l r s))

{- | Pclutch.  Sc3 sample and hold pattern.  For true values in the
control pattern, step the value pattern, else hold the previous value.

> > c = Pseq([1,0,1,0,0,1,1],inf);
> > p = Pclutch(Pser([1,2,3,4,5],8),c);
> > r = [1,1,2,2,2,3,4,5,5,1,1,1,2,3];
> > p.asStream.all == r

>>> let c = pbool (pseq [1,0,1,0,0,1,1] inf)
>>> pclutch (pser [1,2,3,4,5] 8) c == toP [1,1,2,2,2,3,4,5,5,1,1,1,2,3]
True

Note the initialization behavior, nothing is generated until the
first true value.

>>> let p = pseq [1,2,3,4,5] 1
>>> let q = pbool (pseq [0,0,0,0,0,0,1,0,0,1,0,1] 1)
>>> pclutch p q == toP [1,1,1,2,2,3]
True
-}
pclutch :: P a -> P Bool -> P a
pclutch p q =
  let r = fmap (+ 1) (pcountpost q)
  in pstutter r p

{- | Pcollect.  Sc3 name for 'fmap', ie. patterns are functors.

> > Pcollect({|i| i * 3},Pseq(#[1,2,3],1)).asStream.all == [3,6,9]
> > Pseq(#[1,2,3],1).collect({|i| i * 3}).asStream.all == [3,6,9]

>>> pcollect (* 3) (toP [1,2,3]) == toP [3,6,9]
True

>>> fmap (* 3) (toP [1,2,3]) == toP [3,6,9]
True
-}
pcollect :: (a -> b) -> P a -> P b
pcollect = fmap

{- | Pconst.  Sc3 pattern to constrain the sum of a numerical pattern.
Is equal to /p/ until the accumulated sum is within /t/ of /n/.  At
that point, the difference between the specified sum and the
accumulated sum concludes the pattern.

> > p = Pconst(10,Pseed(Pn(1000,1),Prand([1,2,0.5,0.1],inf),0.001));
> > p.asStream.all == [0.5,0.1,0.5,1,2,2,0.5,1,0.5,1,0.9]

>>> let p = pconst 10 (prand 'α' [1,2,0.5,0.1] inf) 0.001
>>> Data.Foldable.sum p
10.0
-}
pconst :: (Ord a, Num a) => a -> P a -> a -> P a
pconst n p t =
  let f _ [] = []
      f j (i : is) =
        if i + j < n - t
          then i : f (j + i) is
          else [n - j]
  in toP (f 0 (unP p))

{- | PdegreeToKey.  Sc3 pattern to derive notes from an index into a
scale.

>>> let p = pseq [0,1,2,3,4,3,2,1,0,2,4,7,4,2] 2
>>> let q = pure [0,2,4,5,7,9,11]
>>> pdegreeToKey p q (pure 12) == toP [0,2,4,5,7,5,4,2,0,4,7,12,7,4,0,2,4,5,7,5,4,2,0,4,7,12,7,4]
True

>>> let p = pseq [0,1,2,3,4,3,2,1,0,2,4,7,4,2] 2
>>> let q = pseq (map return [[0,2,4,5,7,9,11],[0,2,3,5,7,8,11]]) 1
>>> let r = [0,2,4,5,7,5,4,2,0,4,7,12,7,4,0,2,3,5,7,5,3,2,0,3,7,12,7,3]
>>> pdegreeToKey p (pstutter 14 q) (pure 12) == toP r
True

This is the pattern variant of 'Math.degreeToKey'.

>>> let s = [0,2,4,5,7,9,11]
>>> map (Math.degreeToKey s 12) [0,2,4,7,4,2,0] == [0,4,7,12,7,4,0]
True
-}
pdegreeToKey :: (RealFrac a) => P a -> P [a] -> P a -> P a
pdegreeToKey = pzipWith3 (\i j k -> Math.degreeToKey j k i)

{- | Pdiff.  Sc3 pattern to calculate adjacent element difference.

> > Pdiff(Pseq([0,2,3,5,6,8,9],1)).asStream.all == [2,1,2,1,2,1]

>>> pdiff (pseq [0,2,3,5,6,8,9] 1) == toP [2,1,2,1,2,1]
True
-}
pdiff :: Num n => P n -> P n
pdiff p = ptail p - p

{- | Pdrop.  Lifted 'drop'.

> > p = Pseries(1,1,20).drop(5);
> > p.asStream.all == [6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

>>> pdrop 5 (pseries 1 1 10) == toP [6,7,8,9,10]
True

>>> pdrop 1 mempty == mempty
True
-}
pdrop :: Int -> P a -> P a
pdrop n = liftP (drop n)

{- | PdurStutter.  Lifted 'List.durStutter'.

> > s = Pseq(#[1,1,1,1,1,2,2,2,2,2,0,1,3,4,0],inf);
> > d = Pseq(#[0.5,1,2,0.25,0.25],1);
> > PdurStutter(s,d).asStream.all == [0.5,1,2,0.25,0.25]

>>> let s = pseq [1,1,1,1,1,2,2,2,2,2,0,1,3,4,0] inf
>>> let d = pseq [0.5,1,2,0.25,0.25] 1
>>> pdurStutter s d == toP [0.5,1.0,2.0,0.25,0.25]
True
-}
pdurStutter :: Fractional a => P Int -> P a -> P a
pdurStutter = liftP2 List.durStutter

{- | Pexprand.  Lifted 'List.exprand'.

> > Pexprand(0.0001,1,10).asStream.all

>>> pexprand 'α' 0.0001 1 3 == toP [0.8659855714087309,0.41335047782072265,1.5897688002931403e-4]
True

> > Pbind(\freq,Pexprand(0.0001,1,inf) * 600 + 300,\dur,0.02).play

> paudition (pbind [(K_freq,pexprand 'α' 0.0001 1 inf * 600 + 300),(K_dur,0.02)])
-}
pexprand :: (Enum e, Random a, Floating a) => e -> a -> a -> Int -> P a
pexprand e l r = toP . List.exprand e l r

{- | Pfinval.  Alias for 'ptake'

> > Pfinval(5,Pseq(#[1,2,3],inf)).asStream.all == [1,2,3,1,2]

>>> pfinval 5 (pseq [1,2,3] inf) == toP [1,2,3,1,2]
True
-}
pfinval :: Int -> P a -> P a
pfinval = ptake

{- | A variant of the Sc3 pattern that evaluates a closure at each step.
The haskell variant function has a 'StdGen' form.
-}
pfuncn :: Enum e => e -> (StdGen -> (n, StdGen)) -> Int -> P n
pfuncn e f n = toP (List.funcn e f n)

{- | Pgeom.  Sc3 geometric series pattern.

> > Pgeom(3,6,5).asStream.all == [3,18,108,648,3888]

>>> pgeom 3 6 5 == toP [3,18,108,648,3888]
True

> > Pgeom(1,2,10).asStream.all == [1,2,4,8,16,32,64,128,256,512]

>>> pgeom 1 2 10 == toP [1,2,4,8,16,32,64,128,256,512]
True

Real numbers work as well.

> > p = Pgeom(1.0,1.1,6).collect({|i| (i * 100).floor});
> > p.asStream.all == [100,110,121,133,146,161];

>>> let p = fmap (floor . (* 100)) (pgeom 1.0 1.1 6)
>>> p == toP [100,110,121,133,146,161]
True

There is a list variant.

> > 5.geom(3,6)

>>> C.geom 5 3 6 == [3,18,108,648,3888]
True
-}
pgeom :: (Num a) => a -> a -> Int -> P a
pgeom i s n = toP (C.geom n i s)

{- | Pif.  Sc3 /implicitly repeating/ pattern-based conditional expression.

> > a = Pfunc({0.3.coin});
> > b = Pwhite(0,9,3);
> > c = Pwhite(10,19,3);
> > Pfin(9,Pif(a,b,c)).asStream.all

>>> let a = fmap (< 0.75) (pwhite 'α' 0.0 1.0 inf)
>>> let b = pwhite 'β' 0 9 6
>>> let c = pwhite 'γ' 10 19 6
>>> pif a b c * (-1) == toP [-11,-12,-4,0,-1,-17,-14,-15,-7,-3,-12,-8]
True
-}
pif :: P Bool -> P a -> P a -> P a
pif = liftP3_repeat List.if_demand

{- | Place.  Sc3 interlaced embedding of subarrays.

> > Place([0,[1,2],[3,4,5]],3).asStream.all == [0,1,3,0,2,4,0,1,5]

>>> C.lace 9 [[0],[1,2],[3,4,5]]
[0,1,3,0,2,4,0,1,5]

>>> place [[0],[1,2],[3,4,5]] 3 == toP [0,1,3,0,2,4,0,1,5]
True

> > Place(#[1,[2,5],[3,6]],2).asStream.all == [1,2,3,1,5,6]

>>> C.lace 6 [[1],[2,5],[3,6]]
[1,2,3,1,5,6]

>>> place [[1],[2,5],[3,6]] 2 == toP [1,2,3,1,5,6]
True

>>> C.lace 12 [[1],[2,5],[3,6..]]
[1,2,3,1,5,6,1,2,9,1,5,12]

>>> place [[1],[2,5],[3,6..]] 4 == toP [1,2,3,1,5,6,1,2,9,1,5,12]
True
-}
place :: [[a]] -> Int -> P a
place a n =
  let f = toP . concat . take_inf n . transpose . map cycle
  in f a

{- | Pn.
Sc3 pattern to repeat the enclosed pattern a number of times.

>>> pn 1 4 == toP [1,1,1,1]
True

>>> pn (toP [1,2,3]) 3 == toP [1,2,3,1,2,3,1,2,3]
True

This is related to `concat`.`replicate` in standard list processing.

>>> concat (replicate 4 [1])
[1,1,1,1]

>>> concat (replicate 3 [1,2,3])
[1,2,3,1,2,3,1,2,3]

There is a `pconcatReplicate` near-alias (reversed argument order).

>>> pconcatReplicate 4 1 == toP [1,1,1,1]
True

>>> pconcatReplicate 3 (toP [1,2]) == toP [1,2,1,2,1,2]
True

This is productive over infinite lists.

> concat (replicate inf [1])
> pconcat (replicate inf 1)
> pconcatReplicate inf 1
-}
pn :: P a -> Int -> P a
pn p n = mconcat (replicate n p)

{- | Ppatlace.  Sc3 /implicitly repeating/ pattern to lace input patterns.

> > p = Ppatlace([1,Pseq([2,3],2),4],5);
> > p.asStream.all == [1,2,4,1,3,4,1,2,4,1,3,4,1,4]

>>> ppatlace [1,pseq [2,3] 2,4] 5 == toP [1,2,4,1,3,4,1,2,4,1,3,4,1,4]
True

> > p = Ppatlace([1,Pseed(Pn(1000,1),Prand([2,3],inf))],5);
> > p.asStream.all == [1,3,1,3,1,3,1,2,1,2]

>>> ppatlace [1,prand 'α' [2,3] inf] 5 == toP [1,2,1,3,1,3,1,3,1,3]
True

>>> ppatlace [1,toP [2],toP [3,4]] 3 == toP [1,2,3,1,4,1]
True
-}
ppatlace :: [P a] -> Int -> P a
ppatlace a n =
  let a' = transpose (map unP_repeat a)
  in toP (concat (take_inf n a'))

{- | Prand.  Sc3 pattern to make n random selections from a list of
patterns, the resulting pattern is flattened (joined).

> > p = Pseed(Pn(1000,1),Prand([1,Pseq([10,20,30]),2,3,4,5],6));
> > p.asStream.all == [3,5,3,10,20,30,2,2]

>>> prand 'α' [1,toP [10,20],2,3,4,5] 5 == toP [1,10,20,5,10,20,10,20]
True

Random notes:

> > Pbind(\note,Prand([0,1,5,7],inf),\dur,0.25).play

> paudition (pbind [(K_note,prand 'α' [0,1,5,7] inf),(K_dur,0.25)])

The below cannot be written as intended with the list
based pattern library.  This is precisely because the
noise patterns are values, not processes with a state
threaded non-locally.

> do {n0 <- Sound.Sc3.Lang.Random.IO.rrand 2 5
>    ;n1 <- Sound.Sc3.Lang.Random.IO.rrand 3 9
>    ;let p = pseq [prand 'α' [pempty,pseq [24,31,36,43,48,55] 1] 1
>                  ,pseq [60,prand 'β' [63,65] 1
>                        ,67,prand 'γ' [70,72,74] 1] n0
>                  ,prand 'δ' [74,75,77,79,81] n1] inf
>     in return (ptake 24 p)}
-}
prand :: Enum e => e -> [P a] -> Int -> P a
prand = join .:: prand'

{- | Preject.  Sc3 pattern to rejects values for which the predicate
is true.  reject f is equal to filter (not . f).

>>> preject (== 1) (pseq [1,2,3] 2) == toP [2,3,2,3]
True

>>> pfilter (not . (== 1)) (pseq [1,2,3] 2) == toP [2,3,2,3]
True

> > p = Pseed(Pn(1000,1),Pwhite(0,255,20).reject({|x| x.odd}));
> > p.asStream.all == [224,60,88,94,42,32,110,24,122,172]

>>> preject odd (pwhite 'α' 0 255 10) == toP [152,174,74,218]
True

> > p = Pseed(Pn(1000,1),Pwhite(0,255,20).select({|x| x.odd}));
> > p.asStream.all == [151,157,187,129,45,245,101,79,77,243]

>>> pselect odd (pwhite 'α' 0 255 10) == toP [49,157,23,81,169,159]
True
-}
preject :: (a -> Bool) -> P a -> P a
preject f = liftP (filter (not . f))

{- | Prorate.  Sc3 /implicitly repeating/ sub-dividing pattern.

> > p = Prorate(Pseq([0.35,0.5,0.8]),1);
> > p.asStream.all == [0.35,0.65,0.5,0.5,0.8,0.2];

>>> let p = prorate (fmap Left (pseq [0.35,0.5,0.8] 1)) 1
>>> fmap roundE (p * 100) == toP [35,65,50,50,80,20]
True

> > p = Prorate(Pseq([0.35,0.5,0.8]),Pseed(Pn(100,1),Prand([20,1],inf)));
> > p.asStream.all == [7,13,0.5,0.5,16,4]

>>> let p = prorate (fmap Left (pseq [0.35,0.5,0.8] 1)) (prand 'α' [20,1] 3)
>>> fmap roundE (p * 100) == toP [700,1300,50,50,80,20]
True

> > l = [[1,2],[5,7],[4,8,9]].collect(_.normalizeSum);
> > Prorate(Pseq(l,1)).asStream.all

>>> import Sound.Sc3.Common.Buffer
>>> let l = map (Right . normalizeSum) [[1,2],[5,7],[4,8,9]]
>>> prorate (toP l) 1 == toP [0.3333333333333333,0.6666666666666666,0.4166666666666667,0.5833333333333334,0.19047619047619047,0.38095238095238093,0.42857142857142855]
True

> > Pfinval(5,Prorate(0.6,0.5)).asStream.all == [0.3,0.2,0.3,0.2,0.3]

>>> pfinval 5 (prorate (fmap Left 0.6) 0.5) == toP [0.3,0.2,0.3,0.2,0.3]
True
-}
prorate :: Num a => P (Either a [a]) -> P a -> P a
prorate = pjoin_repeat .: pzipWith prorate'

{- | Pselect.  See 'pfilter'.

>>> pselect (< 3) (pseq [1,2,3] 2) == toP [1,2,1,2]
True
-}
pselect :: (a -> Bool) -> P a -> P a
pselect f = liftP (filter f)

{- | Pseq.  Sc3 pattern to cycle over a list of patterns. The repeats
pattern gives the number of times to repeat the entire list.

>>> pseq [return 1,return 2,return 3] 2 == toP [1,2,3,1,2,3]
True

>>> pseq [1,2,3] 2 == toP [1,2,3,1,2,3]
True

>>> pseq [1,pn 2 2,3] 2 == toP [1,2,2,3,1,2,2,3]
True

There is an 'inf' value for the repeats variable.

>>> ptake 3 (pdrop (10^5) (pseq [1,2,3] inf)) == toP [2,3,1]
True

Unlike the Sc3 Pseq, `pseq` does not have an offset argument to give a
starting offset into the list.

>>> pseq (C.rotate 3 [1,2,3,4]) 3 == toP [2,3,4,1,2,3,4,1,2,3,4,1]
True
-}
pseq :: [P a] -> Int -> P a
pseq a i =
  let a' = mconcat a
  in if i == inf then pcycle a' else pn a' i

{- | Pser.  Sc3 pattern that is like 'pseq', however the repeats
variable gives the number of elements in the sequence, not the
number of cycles of the pattern.

>>> pser [1,2,3] 5 == toP [1,2,3,1,2]
True

>>> pser [1,pser [10,20] 3,3] 9 == toP [1,10,20,10,3,1,10,20,10]
True

>>> pser [1,2,3] 5 * 3 == toP [3,6,9,3,6]
True
-}
pser :: [P a] -> Int -> P a
pser a i = ptake i (pcycle (mconcat a))

{- | Pseries.  Sc3 arithmetric series pattern, see also 'pgeom'.

>>> pseries 0 2 10 == toP [0,2,4,6,8,10,12,14,16,18]
True

>>> pseries 9 (-1) 10 == toP [9,8 .. 0]
True

>>> pseries 1.0 0.2 3 == toP [1.0::Double,1.2,1.4]
True
-}
pseries :: (Num a) => a -> a -> Int -> P a
pseries i s n = toP (C.series n i s)

{- | Pshuf.  Sc3 pattern to return @n@ repetitions of a shuffled sequence.

> > Pshuf([1,2,3,4],2).asStream.all

>>> pshuf 'α' [1,2,3,4] 2 == toP [1,3,4,2,1,3,4,2]
True

> > Pbind(\degree,Pshuf([0,1,2,4,5],inf),\dur,0.25).play

> paudition (pbind [(K_degree,pshuf 'α' [0,1,2,4,5] inf),(K_dur,0.25)])
-}
pshuf :: Enum e => e -> [a] -> Int -> P a
pshuf e a =
  let (a', _) = Gen.scramble a (mkStdGen (fromEnum e))
  in pn (toP a')

{- | Pslide.  Lifted 'List.slide'.

> > Pslide([1,2,3,4],inf,3,1,0).asStream.all

>>> pslide [1,2,3,4] 4 3 1 0 True == toP [1,2,3,2,3,4,3,4,1,4,1,2]
True

>>> pslide [1,2,3,4,5] 3 3 (-1) 0 True == toP [1,2,3,5,1,2,4,5,1]
True
-}
pslide :: [a] -> Int -> Int -> Int -> Int -> Bool -> P a
pslide = toP .::::: List.slide

{- | Pstutter.
Sc3 /implicitly repeating/ pattern to repeat each element of a pattern /n/ times.

> > Pstutter(2,Pseq([1,2,3],1)).asStream.all == [1,1,2,2,3,3]

>>> pstutter 2 (pseq [1,2,3] 1) == toP [1,1,2,2,3,3]
True

The count input may be a pattern.

>>> let p = pseq [1,2] inf
>>> let q = pseq [1,2,3] 2
>>> pstutter p q == toP [1,2,2,3,1,1,2,3,3]
True

>>> pstutter (toP [1,2,3]) (toP [4,5,6]) == toP [4,5,5,6,6,6]
True

>>> pstutter 2 (toP [4,5,6]) == toP [4,4,5,5,6,6]
True
-}
pstutter :: P Int -> P a -> P a
pstutter = liftP2_repeat List.stutter

{- | Pswitch.  Lifted 'List.switch'.

>>> let p = pswitch [pseq [1,2,3] 2,pseq [65,76] 1,800] (toP [2,2,0,1])
>>> p == toP [800,800,1,2,3,1,2,3,65,76]
True
-}
pswitch :: [P a] -> P Int -> P a
pswitch l = liftP (List.switch (map unP l))

{- | Pswitch1.  Lifted /implicitly repeating/ 'List.switch1'.

> > l = [Pseq([1,2,3],inf),Pseq([65,76],inf),8];
> > p = Pswitch1(l,Pseq([2,2,0,1],3));
> > p.asStream.all == [8,8,1,65,8,8,2,76,8,8,3,65];

> let p = pswitch1 [pseq [1,2,3] inf
>                  ,pseq [65,76] inf
>                  ,8] (pseq [2,2,0,1] 6)
> in p == toP [8,8,1,65,8,8,2,76,8,8,3,65,8,8,1,76,8,8,2,65,8,8,3,76]
-}
pswitch1 :: [P a] -> P Int -> P a
pswitch1 l = liftP (List.switch1 (map unP_repeat l))

{- | Ptuple.  'pseq' of 'ptranspose_st_repeat'.

> > l = [Pseries(7,-1,8),3,Pseq([9,7,4,2],1),Pseq([4,2,0,0,-3],1)];
> > p = Ptuple(l,1);
> > p.asStream.all == [[7,3,9,4],[6,3,7,2],[5,3,4,0],[4,3,2,0]]

> let p = ptuple [pseries 7 (-1) 8
>                ,3
>                ,pseq [9,7,4,2] 1
>                ,pseq [4,2,0,0,-3] 1] 1
> in p == toP [[7,3,9,4],[6,3,7,2],[5,3,4,0],[4,3,2,0]]
-}
ptuple :: [P a] -> Int -> P [a]
ptuple p = pseq [ptranspose_st_repeat p]

{- | Pwhite.  Lifted 'List.white'.

> pwhite 'α' 0 9 5 == toP [3,0,1,6,6]
> pwhite 'α' 0 9 5 - pwhite 'α' 0 9 5 == toP [0,0,0,0,0]
-}
pwhite :: (Random n, Enum e) => e -> n -> n -> Int -> P n
pwhite = toP .::: List.white

{- | Pwrand.  Lifted 'List.wrand'.

> let w = C.normalizeSum [12,6,3]
> in pwrand 'α' [1,2,3] w 6 == toP [2,1,2,3,3,2]

> > r = Pwrand.new([1,2,Pseq([3,4],1)],[1,3,5].normalizeSum,6);
> > p = Pseed(Pn(100,1),r);
> > p.asStream.all == [2,3,4,1,3,4,3,4,2]

> let w = C.normalizeSum [1,3,5]
> in pwrand 'ζ' [1,2,pseq [3,4] 1] w 6 == toP [3,4,2,2,3,4,1,3,4]
-}
pwrand :: (Enum e) => e -> [P a] -> [Double] -> Int -> P a
pwrand e a w = toP . List.wrand e (map unP a) w

{- | Pwrap.  Type specialised 'List.fwrap', see also 'pfold'.

> > p = Pwrap(Pgeom(200,1.25,9),200,1000.0);
> > r = p.asStream.all.collect({|n| n.round});
> > r == [200,250,313,391,488,610,763,954,392];

>>> let p = fmap roundE (pwrap (pgeom 200 1.25 9) 200 1000)
>>> p == toP [200,250,312,391,488,610,763,954,391]
True
-}
pwrap :: (Ord a, Num a) => P a -> a -> a -> P a
pwrap = List.fwrap

{- | Pxrand.  Lifted 'List.xrand'.

>>> pxrand 'α' [1,toP [2,3],toP [4,5,6]] 9 == toP [1,2,3,4,5,6,2,3,4]
True

> > Pbind(\note,Pxrand([0,1,5,7],inf),\dur,0.25).play

> paudition (pbind [(K_note,pxrand 'α' [0,1,5,7] inf),(K_dur,0.25)])
-}
pxrand :: Enum e => e -> [P a] -> Int -> P a
pxrand e a n = toP (List.xrand e (map unP a) n)

-- * Variant Sc3 Patterns

{- | Lifted /implicitly repeating/ 'List.pbrown''.

>>> pbrown' 'α' 1 700 (pseq [1,20] inf) 4 == toP [665,674,674,664]
True
-}
pbrown' ::
  (Enum e, Random n, Num n, Ord n) =>
  e ->
  P n ->
  P n ->
  P n ->
  Int ->
  P n
pbrown' e l r s n =
  let f = liftP3_repeat (Stream.brown e)
  in ptake n (f l r s)

{- | Un-joined variant of 'prand'.

> prand' 'α' [1,toP [2,3],toP [4,5,6]] 5
-}
prand' :: Enum e => e -> [P a] -> Int -> P (P a)
prand' e a n = toP (List.rand e a n)

{- | Underlying pattern for 'prorate'.

>>> prorate' (Left 0.6) 0.5 == toP [0.3,0.2]
True
-}
prorate' :: Num a => Either a [a] -> a -> P a
prorate' p =
  case p of
    Left p' -> toP . List.rorate_n' p'
    Right p' -> toP . List.rorate_l' p'

{- | Pseq1.
Variant of `pseq` that retrieves only one value from each pattern
on each list traversal.  Compare to `pswitch1`.

>>> pseq [pseq [1,2] 1,pseq [3,4] 1] 2 == toP [1,2,3,4,1,2,3,4]
True

>>> pseq1 [pseq [1,2] 1,pseq [3,4] 1] 2 == toP [1,3,2,4]
True

>>> pseq1 [pseq [1,2] inf,pseq [3,4] inf] 3 == toP [1,3,2,4,1,3]
True
-}
pseq1 :: [P a] -> Int -> P a
pseq1 a i = join (ptake i (pflop a))

{- | A variant of 'pseq' to aid translating a common Sc3 idiom where a
finite random pattern is included in a @Pseq@ list.  In the Sc3
case, at each iteration a new computation is run.  This idiom does
not directly translate to the declarative haskell pattern library.

> > Pseq([1,Prand([2,3],1)],5).asStream.all

>>> pseq [1,prand 'α' [2,3] 1] 5 == toP [1,2,1,2,1,2,1,2,1,2]
True

Although the intended pattern can usually be expressed using an
alternate construction:

> > Pseq([1,Prand([2,3],1)],5).asStream.all

>>> ppatlace [1,prand 'α' [2,3] inf] 5 == toP [1,2,1,3,1,3,1,3,1,3]
True

the 'pseqn' variant handles many common cases.

> > Pseq([Pn(8,2),Pwhite(9,16,1)],5).asStream.all

>>> let p = pseqn [2,1] [8,pwhite 'α' 9 16 inf] 5
>>> p == toP [8,8,9,8,8,10,8,8,14,8,8,16,8,8,10]
True
-}
pseqn :: [Int] -> [P a] -> Int -> P a
pseqn n q =
  let rec p c =
        if c == 0
          then mempty
          else
            let (i, j) = unzip (zipWith psplitAt n p)
            in mconcat i <> rec j (c - 1)
  in rec (map pcycle q)

{- | Pseqr.

A variant of 'pseq' that passes a new seed at each invocation,
see also 'pfuncn'.

> > pseqr (\e -> [pshuf e [1,2,3,4] 1]) 2 == toP [2,3,4,1,4,1,2,3]

> let d = pseqr (\e -> [pshuf e [-7,-3,0,2,4,7] 4,pseq [0,1,2,3,4,5,6,7] 1]) inf
> paudition (pbind [(K_degree,d),(K_dur,0.15)])

> > Pbind(\dur,0.2,
> >       \midinote,Pseq([Pshuf(#[60,61,62,63,64,65,66,67],3)],inf)).play

> let m = pseqr (\e -> [pshuf e [60,61,62,63,64,65,66,67] 3]) inf
> paudition (pbind [(K_dur,0.2),(K_midinote,m)])
-}
pseqr :: (Int -> [P a]) -> Int -> P a
pseqr f n = mconcat (concatMap f [1 .. n])

{- | Variant of 'pser' that consumes sub-patterns one element per
iteration.

>>> pser1 [1,pser [10,20] 3,3] 9 == toP [1,10,3,1,20,3,1,10,3]
True
-}
pser1 :: [P a] -> Int -> P a
pser1 a i = ptake i (join (pflop a))

{- | Lifted /implicitly repeating/ 'List.white_p'.

>>> pwhite_rp 'α' 0 (pseq [9,19] 3) == toP [8,17,7,17,9,10]
True
-}
pwhite_rp :: (Enum e, Random n) => e -> P n -> P n -> P n
pwhite_rp e = liftP2_repeat (List.white_p e)

{- | Lifted 'List.whitei'.

>>> pwhitei 'α' 1 9 5 == toP [8,8,1,5,4]
True

> paudition (pbind [(K_degree,pwhitei 'α' 0 8 inf),(K_dur,0.15)])
-}
pwhitei :: (RealFracE n, Random n, Enum e) => e -> n -> n -> Int -> P n
pwhitei = toP .::: List.whitei

-- * Uid variants

-- | 'liftUid' of 'pbrown'.
pbrownM :: (Uid m, Num n, Ord n, Random n) => n -> n -> n -> Int -> m (P n)
pbrownM = liftUid4 pbrown

-- | 'liftUid' of 'pexprand'.
pexprandM :: (Uid m, Random a, Floating a) => a -> a -> Int -> m (P a)
pexprandM = liftUid3 pexprand

-- | 'liftUid' of 'prand'.
prandM :: Uid m => [P a] -> Int -> m (P a)
prandM = liftUid2 prand

-- | 'liftUid' of 'pshuf'.
pshufM :: Uid m => [a] -> Int -> m (P a)
pshufM = liftUid2 pshuf

-- | 'liftUid' of 'pwhite'.
pwhiteM :: (Uid m, Random n) => n -> n -> Int -> m (P n)
pwhiteM = liftUid3 pwhite

-- | 'liftUid' of 'pwhitei'.
pwhiteiM :: (Uid m, RealFracE n, Random n) => n -> n -> Int -> m (P n)
pwhiteiM = liftUid3 pwhitei

-- | 'liftUid' of 'pwrand'.
pwrandM :: Uid m => [P a] -> [Double] -> Int -> m (P a)
pwrandM = liftUid3 pwrand

-- | 'liftUid' of 'pxrand'.
pxrandM :: Uid m => [P a] -> Int -> m (P a)
pxrandM = liftUid2 pxrand
