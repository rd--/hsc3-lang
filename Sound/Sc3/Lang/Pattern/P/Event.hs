{- | @sclang@ event pattern functions.

Sc3 /event/ patterns: `padd` (Padd), `pbind` (Pbind), `pkey`
(Pkey), `pmono` (Pmono), `pmul` (Pmul), `ppar` (Ppar), `pstretch`
(Pstretch), `ptpar` (Ptpar).  `pedit`, `pinstr`, `pmce2`, `psynth`,
`punion`.
-}
module Sound.Sc3.Lang.Pattern.P.Event where

import qualified Data.Foldable as Foldable {- base -}
import Data.Maybe {- base -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import Sound.Sc3.Lang.Control.Duration {- hsc3-lang -}
import Sound.Sc3.Lang.Control.Event {- hsc3-lang -}
import Sound.Sc3.Lang.Control.Instrument {- hsc3-lang -}
import Sound.Sc3.Lang.Core {- hsc3-lang -}
import Sound.Sc3.Lang.Pattern.P {- hsc3-lang -}

-- * Sc3 Event Patterns

-- | NewType for event patterns.
newtype P_Event = P_Event {p_Event :: P Event}

-- | 'P_Event' is audible, 'P' 'Event' could be as well but it'd be an orphan instance.
instance Sc3.Audible P_Event where
  playAt _ = e_play . Event_Seq . unP . p_Event

-- | 'play' of 'P_Event'.
pplay :: Osc.Transport m => P Event -> m ()
pplay = Sc3.play . P_Event

-- | 'audition' of 'P_Event'.
paudition :: P Event -> IO ()
paudition = Sc3.audition . P_Event

-- | Synonym for ('Key','P Field').
type P_Bind = (Key, P Field)

{- | Padd.  Add a value to an existing key, or set the key if it doesn't exist.

> > p = Padd(\freq,801,Pbind(\freq,Pseq([100],1)));
> > p.asStream.all(()) == [('freq':901)]

> let p = padd (K_freq,801) (pbind [(K_freq,return 100)])
> p == pbind [(K_freq,return 901)]

> > Padd(\freq,Pseq([401,801],2),Pbind(\freq,100)).play

> paudition (padd (K_freq,pseq [401,801] 2) (pbind [(K_freq,100)]))
-}
padd :: P_Bind -> P Event -> P Event
padd (k, p) = pzipWith (\i j -> e_edit k 0 (+ i) j) p

{- | Pbind.  Sc3 pattern to assign keys to a set of 'Field' patterns
making an 'Event' pattern.

Each input pattern is assigned a key in the resulting event pattern.

There are a set of reserved keys that have particular roles in the
pattern library, ie. frequency duration and amplitude, and a
generalised _named parameter_ mechanism.

The named keys given by `K_param` are ordinarily synthesiser
parameters, ie. the stereo location of the sound.

'K_param' can be elided if /OverloadedStrings/ are in place.

> :set -XOverloadedStrings

> ptake 2 (pbind [("x",pwhitei 'α' 0 9 inf),("y",pseq [1,2,3] inf)])

'Event's implement variations on the @Sc3@ 'Dur' and
'Sound.Sc3.Lang.Control.Pitch.Pitch' models.
-}
pbind :: [P_Bind] -> P Event
pbind xs =
  let xs' = fmap (\(k, v) -> pzip (undecided k) v) xs
      xs'' = ptranspose_st_repeat xs'
  in fmap e_from_list xs''

{- | Operator to lift 'F_Value' pattern to 'P_Bind' tuple.

> let {r = True `pcons` preplicate 3 False :: P Bool}
> in pbind [K_rest <| r] == pbind [(K_rest,pseq [1,0,0,0] 1)]
-}
(<|) :: F_Value v => Key -> P v -> P_Bind
(<|) k p = (k, fmap toF p)

infixl 3 <|

{- | Pkey.  Sc3 pattern to read 'Key' at 'Event' pattern.

> > p = Pbind(\x,Pseq([1,2,3],1),\y,Pseed(Pn(100,1),Prand([4,5,6],inf)));
> > p.asStream.all(()) == [('y':4,'x':1),('y':6,'x':2),('y':4,'x':3)]

> let p = pbind [(K_param "x",prand 'α' [100,300,200] inf)
>               ,(K_param "y",pseq [1,2,3] 1)]
> in pkey (K_param "x") p == toP [200,200,300]

Note however that in haskell is usually more appropriate to name the
pattern using /let/.

> pkey K_freq (pbind [(K_freq,return 440)]) == toP [440]
> pkey K_amp (pbind [(K_amp,toP [0,1])]) == toP [0,1]
-}
pkey :: Key -> P Event -> P Field
pkey k = fmap (fromJust . e_get k)

{- | Pmono.  Sc3 pattern that is a variant of 'pbind' for controlling
monophonic (persistent) synthesiser nodes.
-}
pmono :: [P_Bind] -> P Event
pmono b =
  let ty = fmap F_String ("s_new" `pcons` prepeat "n_set")
  in pbind ((K_type, ty) : b)

{- | Pmul.  Sc3 pattern to multiply an existing key by a value, or set
the key if it doesn't exist.

> let p = pbind [(K_dur,0.15),(K_freq,prand 'α' [440,550,660] 6)]
> paudition (pseq [p,pmul (K_freq,2) p,pmul (K_freq,0.5) p] 2)
-}
pmul :: P_Bind -> P Event -> P Event
pmul (k, p) = pzipWith (\i j -> e_edit k 1 (* i) j) p

{- | Ppar.  Variant of 'ptpar' with zero start times.

Ordinarily the distance from one event to the next is given by the
/delta/ time of the event.  However this can be set directly by using the
'K_fwd'' key.  A 'K_fwd'' value of zero means that the next event is
simultaneous with the current event.

Setting 'K_fwd'' directly is not normally a good idea, instead use
`pmerge` and `ppar`.

> let a = pbind [(K_param "a",pseq [1,2,3] inf)]
> let b = pbind [(K_param "b",pseq [4,5,6] inf)]
> let r = toP [e_from_list [(K_param "a",1),(K_fwd',0)],e_from_list [(K_param "b",4),(K_fwd',1)]]
> ptake 2 (ppar [a,b]) == r

The result of `pmerge` can be merged again, `ppar` merges a list of
patterns.
-}
ppar :: [P Event] -> P Event
ppar l = ptpar (zip (repeat 0) l)

{- | Pstretch.  Sc3 pattern to do time stretching.  It is equal to
'pmul' at 'K_stretch'.
-}
pstretch :: P Field -> P Event -> P Event
pstretch p = pmul (K_stretch, p)

{- | Ptpar.  Merge a set of 'Event' patterns each with indicated start 'Osc.Time'.

`ptpar` is a variant of `ppar` which allows non-equal start times.
-}
ptpar :: [(Osc.Time, P Event)] -> P Event
ptpar l =
  case l of
    [] -> mempty
    [(_, p)] -> p
    (pt, p) : (qt, q) : r -> ptpar ((min pt qt, ptmerge (pt, p) (qt, q)) : r)

-- * Instrument Event Patterns

{- | Pattern from 'Instr'.  An 'Instr' is either a 'Synthdef' or a
/name/.  In the 'Synthdef' case the instrument is asynchronously
sent to the server before processing the event, which has timing
implications.  The pattern constructed here uses the 'Synthdef' for
the first element, and the subsequently the /name/.
-}
pinstr' :: Instr -> P Field
pinstr' i = toP (map F_Instr (i_repeat i))

-- | 'Instr' pattern from instrument /name/.  See also `psynth`.
pinstr :: String -> P Field
pinstr s = pinstr' (Instr_Ref s True)

-- | `Synthdef`s can be used directly as an instrument using `psynth`.
psynth :: Sc3.Synthdef -> P Field
psynth s = pinstr' (Instr_Def s True)

-- * MCE Patterns

{- | Two-channel MCE for /field/ patterns.

> pmce2 (toP [1,2]) (toP [3,4]) == toP [f_array [1,3],f_array [2,4]]

> let p = pmce2 (pseq [1,2] inf) (pseq [3,4] inf)
> in ptake 2 p == toP [f_array [1,3],f_array [2,4]]
-}
pmce2 :: P Field -> P Field -> P Field
pmce2 = pzipWith (\m n -> F_Vector [m, n])

-- | Three-channel MCE for /field/ patterns.
pmce3 :: P Field -> P Field -> P Field -> P Field
pmce3 = pzipWith3 (\m n o -> F_Vector [m, n, o])

{- | Remove one layer of MCE expansion at an /event/ pattern.

The pattern will be expanded only to the width of the initial input.
Holes are filled with rests.

`p_un_mce` translates via `ppar`.  This allows `dur` related fields to
be MCE values.  The underlying event processor also implements one
layer of MCE expansion.
-}
p_un_mce :: P Event -> P Event
p_un_mce p =
  let l' = transpose_fw_def' e_rest (map e_un_mce' (unP p))
  in toP (e_par (zip (repeat 0) l'))

-- * Non-Sc3 Event Patterns

-- | Edit 'a' at 'Key' in each element of an 'Event' pattern.
pedit :: Key -> (Field -> Field) -> P Event -> P Event
pedit k f = fmap (e_edit' k f)

{- | Pattern of start times of events at event pattern.

> p_time (pbind [(K_dur,toP [1,2,3,2,1])]) == toP [0,1,3,6,8,9]
> p_time (pbind [(K_dur,pseries 0.5 0.5 5)]) == toP [0,0.5,1.5,3,5,7.5]
-}
p_time :: P Event -> P Osc.Time
p_time = pscanl (+) 0 . fmap (fwd . e_dur Nothing)

{- | Pattern to extract 'a's at 'Key' from an 'Event'
pattern.

> pkey_m K_freq (pbind [(K_freq,return 440)]) == toP [Just 440]
-}
pkey_m :: Key -> P Event -> P (Maybe Field)
pkey_m k = fmap (e_get k)

{- | Variant of 'ptmerge' with zero start times.

`pmerge` merges two event streams, adding /fwd'/ entries as required.
-}
pmerge :: P Event -> P Event -> P Event
pmerge p q = ptmerge (0, p) (0, q)

-- | Variant that does not insert key.
pmul' :: P_Bind -> P Event -> P Event
pmul' (k, p) = pzipWith (\i j -> e_edit' k (* i) j) p

-- | Merge two 'Event' patterns with indicated start 'Osc.Time's.
ptmerge :: (Osc.Time, P Event) -> (Osc.Time, P Event) -> P Event
ptmerge (pt, p) (qt, q) =
  toP (e_merge (pt, Foldable.toList p) (qt, Foldable.toList q))

-- | Left-biased union of event patterns.
punion :: P Event -> P Event -> P Event
punion = pzipWith (<>)

-- | 'punion' of 'pbind' of 'return', ie. @p_with (K_Instr,psynth s)@.
p_with :: P_Bind -> P Event -> P Event
p_with = punion . pbind . return

-- * Nrt

{- | Transform an /event/ pattern into a /non-real time/ Sc3 score.

> let n1 = pNrt (pbind [(K_freq,prand 'α' [300,500,231.2,399.2] inf),(K_dur,pseq [0.1,0.2] 3)])
> nrt_audition n1

> mapM_ (putStrLn . bundlePP (Just 4)) (nrt_bundles n1)

Infinite 'Nrt' scores are productive for 'audition'ing.

> let n2 = pNrt (pbind [(K_dur,0.25),(K_freq,pseq [300,600,900] inf)])
> nrt_audition n2
> mapM_ (putStrLn . bundlePP (Just 4)) (take 9 (nrt_bundles n2))
-}
pNrt :: P Event -> Sc3.Nrt
pNrt = e_nrt . Event_Seq . unP
