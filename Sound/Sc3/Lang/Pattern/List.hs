-- | List variants of @Sc3@ pattern functions.
module Sound.Sc3.Lang.Pattern.List where

import Data.List {- base -}
import qualified Data.Map as Map {- containers -}
import System.Random {- random -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Lang.Collection as Collection
import qualified Sound.Sc3.Lang.Core as Core
import qualified Sound.Sc3.Lang.Pattern.Stream as Stream

-- * Data.Bool variants

{- | '>' @0@.  Values greater than zero are 'True'.
Zero and negative values are 'False'.
-}
bool :: (Ord n, Num n) => n -> Bool
bool = (> 0)

-- * Data.Functor variants

{- | 'fmap' of 'bool'.

>>> fbool [2,1,0,-1]
[True,True,False,False]
-}
fbool :: (Ord a, Num a, Functor f) => f a -> f Bool
fbool = fmap bool

{- | Sc3 pattern to fold values to lie within range (as opposed to
wrap and clip).  This is /not/ related to "Data.Foldable".

>>> ffold [10,11,12,-6,-7,-8] (-7) 11
[10,11,10,-6,-7,-6]

The underlying primitive is the 'Sc3.sc3_fold' function.

>>> let f n = Sc3.sc3_fold n (-7) 11
>>> map f [10,11,12,-6,-7,-8]
[10,11,10,-6,-7,-6]
-}
ffold :: (Functor f, Num a, Ord a) => f a -> a -> a -> f a
ffold p i j = fmap (\n -> Sc3.sc3_fold n i j) p

{- | Sc3 pattern to constrain the range of output values by wrapping,
the primitive is 'Sc3.generic_wrap'.

>>> fmap round (fwrap (geom 200 1.2 10) 200 1000)
[200,240,288,346,415,498,597,717,860,231]
-}
fwrap :: (Functor f, Ord a, Num a) => f a -> a -> a -> f a
fwrap xs l r = fmap (Sc3.generic_wrap (l, r)) xs

-- * Non-Sc3 Patterns

{- | Count the number of `False` values following each `True` value.

>>> countpost (map bool [1,0,1,0,0,0,1,1])
[1,3,0,0]
-}
countpost :: [Bool] -> [Int]
countpost =
  let f i p =
        if null p
          then [i]
          else
            let (x : xs) = p
                r = i : f 0 xs
            in if not x then f (i + 1) xs else r
  in tail . f 0

{- | Count the number of `False` values preceding each `True` value.

>>> countpre (fbool [0,0,1,0,0,0,1,1])
[2,3,0]
-}
countpre :: [Bool] -> [Int]
countpre =
  let f i p =
        if null p
          then if i == 0 then [] else [i]
          else
            let (x : xs) = p
                r = i : f 0 xs
            in if x then r else f (i + 1) xs
  in f 0

{- | Sample and hold initial value.

>>> hold []
[]

>>> hold [1..5]
[1,1,1,1,1]

>>> hold [1,undefined]
[1,1]
-}
hold :: [a] -> [a]
hold l =
  case l of
    [] -> []
    e : _ -> map (const e) l

{- | Interleave elements from two lists.  If one list ends the other
continues until it also ends.

>>> interleave2 [1,2,3,1,2,3] [4,5,6,7]
[1,4,2,5,3,6,1,7,2,3]

>>> [1..9] `isPrefixOf` interleave2 [1,3..] [2,4..]
True
-}
interleave2 :: [a] -> [a] -> [a]
interleave2 p q =
  case (p, q) of
    ([], _) -> q
    (_, []) -> p
    (x : xs, y : ys) -> x : y : interleave2 xs ys

{- | N-ary variant of 'interleave2', ie. 'concat' of 'transpose'.

>>> interleave [whitei 'α' 0 4 3,whitei 'β' 5 9 3]
[3.0,7.0,3.0,8.0,0.0,8.0]

>>> [1..9] `isPrefixOf` interleave [[1,4..],[2,5..],[3,6..]]
True
-}
interleave :: [[a]] -> [a]
interleave = concat . transpose

{- | Pattern where the 'tr' pattern determines the rate at which
values are read from the `x` pattern.  For each sucessive true
value at 'tr' the output is a (`Just` e) of each succesive element at
x.  False values at 'tr' generate `Nothing` values.

>>> trigger (map toEnum [0,1,0,0,1,1]) [1,2,3]
[Nothing,Just 1,Nothing,Nothing,Just 2,Just 3]
-}
trigger :: [Bool] -> [a] -> [Maybe a]
trigger p q =
  let r = countpre p
      f i x = replicate i Nothing ++ [Just x]
  in concat (Collection.zipWith_c f r q)

-- * Sc3 Patterns

{- | Pbrown.  Sc3 pattern to generate psuedo-brownian motion.

>>> take 5 $ brown 'α' 0 9 15
[8,4,6,8,7]
-}
brown :: (Enum e, Random n, Num n, Ord n) => e -> n -> n -> n -> [n]
brown e l r s = Stream.brown e (repeat l) (repeat r) (repeat s)

{- | PdurStutter.  Sc3 pattern to partition a value into /n/ equal
subdivisions.  Subdivides each duration by each stutter and yields
that value stutter times.  A stutter of @0@ will skip the duration
value, a stutter of @1@ yields the duration value unaffected.

>>> durStutter [1,1,1,1,1,2,2,2,2,2,0,1,3,4,0] [0.5,1,2,0.25,0.25]
[0.5,1.0,2.0,0.25,0.25]
-}
durStutter :: Fractional a => [Int] -> [a] -> [a]
durStutter l =
  let f s d = case s of
        0 -> []
        1 -> [d]
        _ -> replicate s (d / fromIntegral s)
  in concat . zipWith f l

{- | Pexprand.
Sc3 pattern of random values that follow a exponential distribution.

>>> exprand 'α' 0.0001 1 4
[0.8659855714087309,0.41335047782072265,1.5897688002931403e-4,2.910352400936899e-2]
-}
exprand :: (Enum e, Random a, Floating a) => e -> a -> a -> Int -> [a]
exprand e l r n = Core.take_inf n (Stream.exprand e l r)

{- | Pfuncn.  Variant of the Sc3 pattern that evaluates a closure at
each step that has a 'StdGen' form.
-}
funcn :: Enum e => e -> (StdGen -> (n, StdGen)) -> Int -> [n]
funcn e f n = Core.take_inf n (Stream.fiter (mkStdGen (fromEnum e)) f)

{- | Pgeom.  'Collection.geom' with arguments re-ordered.

>>> geom 3 6 5
[3,18,108,648,3888]
-}
geom :: Num a => a -> a -> Int -> [a]
geom i s n = Collection.geom n i s

{- | Pif.  Consume values from /q/ or /r/ according to /p/.

>>> if_demand [True,False,True] [1,3] [2]
[1,2,3]
-}
if_demand :: [Bool] -> [a] -> [a] -> [a]
if_demand p q r =
  case if_rec (p, q, r) of
    Just (e, (p', q', r')) -> e : if_demand p' q' r'
    Nothing -> []

{- | Pseq.  'concat' of 'replicate' of 'concat'.

>>> seq' [return 1,[2,3],return 4] 2
[1,2,3,4,1,2,3,4]
-}
seq' :: [[a]] -> Int -> [a]
seq' l n = concat (replicate n (concat l))

{- | Pslide.  Sc3 pattern to slide over a list of values.

>>> slide [1,2,3,4] 4 3 1 0 True
[1,2,3,2,3,4,3,4,1,4,1,2]

>>> slide [1,2,3,4,5] 3 3 (-1) 0 True
[1,2,3,5,1,2,4,5,1]
-}
slide :: [a] -> Int -> Int -> Int -> Int -> Bool -> [a]
slide a n j s i wr = concat (take n (Stream.slide a j s i wr))

{- | Pstutter.  Repeat each element of a pattern /n/ times.

>> stutter [1,2,3] [4,5,6]
[4,5,5,6,6,6]

>>> stutter (repeat 2) [4,5,6]
[4,4,5,5,6,6]
-}
stutter :: [Int] -> [a] -> [a]
stutter l = concat . zipWith replicate l

{- | Pswitch.
Sc3 pattern to select elements from a list of patterns by a pattern of indices.

>>> putStrLn $ switch ["αβ","γδ","εζ"] [1,1,0,2,1,0,0]
γδγδαβεζγδαβαβ

>>> switch [[1,2,3,1,2,3],[5,6],[8]] [2,2,0,1]
[8,8,1,2,3,1,2,3,5,6]
-}
switch :: [[a]] -> [Int] -> [a]
switch l i = i >>= (l !!)

{- | Pswitch1.  Sc3 pattern that uses a pattern of indices to select
which pattern to retrieve the next value from.  Only one value is
selected from each pattern.  This is in comparison to 'switch',
which embeds the pattern in its entirety.

>>> switch1 [(cycle [1,2,3]),(cycle [65,76]),repeat 8] (concat (replicate 6 [2,2,0,1]))
[8,8,1,65,8,8,2,76,8,8,3,65,8,8,1,76,8,8,2,65,8,8,3,76]
-}
switch1 :: [[a]] -> [Int] -> [a]
switch1 ps =
  let rec m l =
        case l of
          [] -> []
          i : l' -> case Map.lookup i m of
            Nothing -> []
            Just [] -> []
            Just (x : xs) -> x : rec (Map.insert i xs m) l'
  in rec (Map.fromList (zip [0 ..] ps))

{- | Pwhite.
Sc3 pattern to generate a uniform linear distribution in given range.

>>> white 'α' 0 9 5
[8,1,7,1,9]

Note that this structure is not actually indeterminate.

>>> white 'α' 1 9 5
[9,2,8,2,1]

>>> let p = white 'α' 0.0 1.0 3 in zipWith (-) p p
[0.0,0.0,0.0]
-}
white :: (Random n, Enum e) => e -> n -> n -> Int -> [n]
white e l r n = Core.take_inf n (Stream.white e l r)

{- | Pwrand.  Sc3 pattern to embed values randomly chosen from a list.
Returns one item from the list at random for each repeat, the
probability for each item is determined by a list of weights which
should sum to 1.0 and must be equal in length to the selection list.

>>> let w = Sc3.normalizeSum [1,3,5]
>>> wrand 'ζ' [[1],[2],[3,4]] w 6
[3,4,1,2,2,2,2]
-}
wrand ::
  (Enum e, Fractional n, Ord n, Random n) =>
  e ->
  [[a]] ->
  [n] ->
  Int ->
  [a]
wrand e a w n = concat (Core.take_inf n (Stream.wrand_generic e a w))

{- | Prand.  /n/ random elements of list.

>>> rand 'α' [1..9] 9
[9,2,8,2,1,8,6,2,4]
-}
rand :: Enum e => e -> [a] -> Int -> [a]
rand e a n = Core.take_inf n (Stream.rand e a)

{- | Pxrand.  Sc3 pattern that is like 'rand' but filters successive duplicates.

> xrand 'α' [[1],[2,3],[4,5,6]] 9 == [4,5,6,2,3,4,5,6,1]
-}
xrand :: Enum e => e -> [[a]] -> Int -> [a]
xrand e a n = Core.take_inf n (concat (Stream.xrand e a))

-- * Sc3 Variant Patterns

-- | Underlying 'if_demand'.
if_rec :: ([Bool], [a], [a]) -> Maybe (a, ([Bool], [a], [a]))
if_rec i =
  case i of
    (True : p, q : q', r) -> Just (q, (p, q', r))
    (False : p, q, r : r') -> Just (r, (p, q, r'))
    _ -> Nothing

{- | 'zip3' variant.

> if_zip [True,False,True] [1,3] [2] == [1]
-}
if_zip :: [Bool] -> [a] -> [a] -> [a]
if_zip a b c =
  let f (x, y, z) = if x then y else z
  in map f (zip3 a b c)

rorate_n' :: Num a => a -> a -> [a]
rorate_n' p i = [i * p, i * (1 - p)]

rorate_n :: Num a => [a] -> [a] -> [a]
rorate_n l = concat . zipWith rorate_n' l

rorate_l' :: Num a => [a] -> a -> [a]
rorate_l' p i = map (* i) p

rorate_l :: Num a => [[a]] -> [a] -> [a]
rorate_l l = concat . zipWith rorate_l' l

{- | 'white' with pattern inputs.

> white_p 'α' (repeat 0) [9,19,9,19,9,19] == [8,17,7,17,9,10]
-}
white_p :: (Enum e, Random n) => e -> [n] -> [n] -> [n]
white_p e l r =
  let g = mkStdGen (fromEnum e)
      n = zip l r
      f a b = let (a', b') = randomR b a in (b', a')
  in snd (mapAccumL f g n)

{- | 'Sc3.floorE' of 'white'

> whitei 'α' 1 9 5 == [8,8,1,5,4]
-}
whitei :: (Random n, Sc3.RealFracE n, Enum e) => e -> n -> n -> Int -> [n]
whitei e l r = fmap Sc3.floorE . white e l r
