{-
-- | @Sc3@ pattern function as "machines".
module Sound.Sc3.Lang.Pattern.Machines where

import Control.Monad {- base -}
import Data.Int {- base -}
import System.Random {- random -}

import qualified Data.Map as Map {- containers -}

import Control.Monad.IO.Class {- transformers -}

import Data.Machine {- machines -}

import Sound.Osc.Core {- hosc -}

type P = MachineT

k_inf :: Int
k_inf = maxBound

{- | Value /x/ once.

> run (p_once 'α') == "α"
-}
p_once :: Monad m => t -> P m k t
p_once x = construct (yield x)

-- | Value /x/ indefinitely.
p_inf :: Monad m => t -> P m k t
p_inf x = repeatedly (yield x)

{- | Pattern /p/ indefinitely.

> run (p_take 7 (p_cycle (p_from_list [1,2,3]))) == [1,2,3,1,2,3,1]
-}
p_cycle :: Monad m => P m k b -> P m k b
p_cycle x = starve x (p_cycle x)

{- | Pattern /x/ /n/ times.

> run (p_n 3 (p_from_list "αβγ")) == "αβγαβγαβγ"
-}
p_n :: Monad m => Int -> P m k t -> P m k t
p_n n x = p_seq (replicate n x)

{- | First /n/ values of pattern /x/.

> run (p_take 3 (p_inf 'α')) == "ααα"
-}
p_take :: Monad m => Int -> P m k c -> P m k c
p_take n x = x ~> taking n

{- | List to pattern.

> run (p_from_list "αβγ") == "αβγ"
-}
p_from_list :: Monad m => [t] -> P m k t
p_from_list = source

-- | Sc3 name for 'fmap'
p_collect :: Monad m => (t -> u) -> P m k t -> P m k u
p_collect = fmap

-- | Sc3 name for 'filter'.
p_select :: Monad m => (t -> Bool) -> P m k t -> P m k t
p_select p x = x ~> filtered p

-- | 'p_select' of 'not'.
p_reject :: Monad m => (t -> Bool) -> P m k t -> P m k t
p_reject p = p_select (not . p)

{- | 'p_reject' of '=='.

> run (p_reject_eq 'β' (p_from_list "αβγ")) == "αγ"
-}
p_reject_eq :: (Monad m, Eq t) => t -> P m k t -> P m k t
p_reject_eq x = p_reject (== x)

{- | zip.

> run (p_tuple_2 (p_from_list "αβγ") (p_from_list "δεζ")) == [('α','δ'),('β','ε'),('γ','ζ')]
-}
p_tuple_2 :: Monad m => P m k t -> P m k u -> P m k (t, u)
p_tuple_2 = teeT (Data.Machine.zipWith (,))

{- | zip3.

> let l = p_from_list
> run (p_tuple_3 (l "αβ") (l "γδ") (l "εζ")) == [('α','γ','ε'),('β','δ','ζ')]
-}
p_tuple_3 :: Monad m => P m k t -> P m k u -> P m k v -> P m k (t, u, v)
p_tuple_3 m n = teeT (Data.Machine.zipWith (\p (q, r) -> (p, q, r))) m . p_tuple_2 n

{- | List of patterns to pattern of lists.

> run (p_tuple (map p_from_list ["αβγ","δεζ","ηθι"])) == ["αδη","βεθ","γζι"]
-}
p_tuple :: Monad m => [P m k t] -> P m k [t]
p_tuple l =
  case l of
    [] -> error "p_tuple: null?"
    [m] -> p_collect return m
    m : l' -> teeT (Data.Machine.zipWith (:)) m (p_tuple l')

{- | Interleave two patterns.

> run (p_lace_2 (p_from_list "αβγ") (p_from_list "δεζ")) == "αδβεγζ"
-}
p_lace_2 :: Monad m => P m k t -> P m k t -> P m k t
p_lace_2 = teeT (repeatedly (awaits L >>= yield >> awaits R >>= yield))

{- | Interleave a list of patterns.

> run (p_lace (map p_from_list ["αβγ","δεζ","ηθι"])) == "αδηβεθγζι"
-}
p_lace :: Monad m => [P m k t] -> P m k t
p_lace l = p_tuple l ~> asParts

{- | Group adjacent elements of input as pairs.

> run (p_adj_2 (p_from_list "αδβεγζ")) == [('α','δ'),('β','ε'),('γ','ζ')]
-}
p_adj_2 :: Monad m => P m k t -> P m k (t, t)
p_adj_2 x = x ~> repeatedly (await >>= \p -> await >>= \q -> yield (p, q))

{- | Group adjacent elements of input as triples.

> run (p_adj_3 (p_from_list "αδβεγζ")) == [('α','δ','β'),('ε','γ','ζ')]
-}
p_adj_3 :: Monad m => P m k t -> P m k (t, t, t)
p_adj_3 x = x ~> repeatedly (await >>= \p -> await >>= \q -> await >>= \r -> yield (p, q, r))

{- | Pattern /p/, then /q/.

> run (p_seq_2 (p_from_list "αβγ") (p_from_list "δεζ")) == "αβγδεζ"
-}
p_seq_2 :: Monad m => P m k t -> P m k t -> P m k t
p_seq_2 = starve

{- | 'foldl' of 'p_seq_2'.

> run (p_seq (map p_from_list ["αβγ","δεζ","ηθι"])) == "αβγδεζηθι"
-}
p_seq :: Monad m => [P m k t] -> P m k t
p_seq = foldl p_seq_2 mempty

{- | Repeat each element of /q/ the number of times indicated at /p/.

> run (p_stutter (p_from_list [2,3,4]) (p_from_list "αβγ")) == "ααβββγγγγ"
-}
p_stutter :: Monad m => P m k Int -> P m k t -> P m k t
p_stutter =
  let f = awaits L >>= \k -> awaits R >>= \x -> replicateM k (yield x)
  in teeT (repeatedly f)

-- | Variant of p_stutter with constant repeat count.
p_duplicate :: Monad m => Int -> P m k t -> P m k t
p_duplicate k m = m ~> repeatedly (await >>= replicateM k . yield)

{- | Console input, character at a time.

> runT (p_take 3 p_getc)
> runT (p_stutter (p_from_list [2,3,4]) (p_take 3 p_getc))
-}
p_getc :: MonadIO m => P m k Char
p_getc = construct (exhaust (fmap Just (liftIO getChar)))

-- | Run /f/ at each step of pattern /x/ and return result.
p_map_m :: Monad m => (t -> PlanT (Is t) u m u) -> MachineT m k t -> MachineT m k u
p_map_m f m = m ~> repeatedly (await >>= (f >=> yield))

p_map_m_ :: Monad m => (t -> PlanT (Is t) t m a) -> MachineT m k t -> MachineT m k t
p_map_m_ f = p_map_m (\x -> f x >> return x)

p_repeat_m :: Monad m => (() -> PlanT (Is ()) t m t) -> MachineT m k t
p_repeat_m f = p_map_m f (p_inf ())

{- | Console output.

> import System.IO
> hSetBuffering stdin NoBuffering
> runT (p_putc (p_duplicate 3 (p_take 3 p_getc)))
-}
p_putc :: MonadIO m => P m k Char -> P m k Char
p_putc = p_map_m_ (liftIO . putChar)

{- | Print values to console.

> r <- runT (p_trace "n=" (p_from_list [1,2,3]))
> r == [1,2,3]
-}
p_trace :: (MonadIO m, Show t) => String -> P m k t -> P m k t
p_trace txt = p_map_m_ (liftIO . putStrLn . (txt ++) . show)

{- | Moving sum of two-element windows.

> run (p_sum_2 (p_from_list [0,1,3,6,10,15])) == [0,1,4,9,16,25]
-}
p_sum_2 :: (Monad m, Fractional t) => P m k t -> P m k t
p_sum_2 m = m ~> auto (unfoldMealy (\x1 x0 -> (x0 + x1, x0)) 0)

{- | Moving average of two-element windows.

> run (p_avg_2 (p_from_list [0,1,3,6,10,15])) == [0,0.5,2,4.5,8,12.5]
-}
p_avg_2 :: (Monad m, Fractional t) => P m k t -> P m k t
p_avg_2 = fmap (/ 2) . p_sum_2

{- | Moving sum of three-element windows.

> run (p_sum_3 (p_from_list [0,1,3,6,10,15])) == [0,1,4,10,19,31]
-}
p_sum_3 :: (Monad m, Fractional t) => P m k t -> P m k t
p_sum_3 m = m ~> auto (unfoldMealy (\(x2, x1) x0 -> (x0 + x1 + x2, (x1, x0))) (0, 0))

{- | Moving average of three-element windows.

> run (p_avg_3 (p_from_list [0,1,3,6,10,15])) == [0,1/3,4/3,10/3,19/3,31/3]
-}
p_avg_3 :: (Monad m, Fractional t) => P m k t -> P m k t
p_avg_3 = fmap (/ 3) . p_sum_3

-- | Linear congruential generator (pseudo–random number generator).
p_lcg_i32 :: Monad m => Int32 -> Int32 -> Int32 -> P m (Is ()) Int32
p_lcg_i32 a c x0 = p_inf () ~> auto (unfoldMoore (\i -> let r = c + i in (r, \_ -> r * a)) x0)

{- | 'p_lcg_i32' with constants from glibc.

> run (p_take 4 (p_lcg_glibc 3678142)) == [3690487,-798917020,-1822590771,-1255598718]
-}
p_lcg_glibc :: Monad m => Int32 -> P m (Is ()) Int32
p_lcg_glibc = p_lcg_i32 1103515245 12345

-- | Type-specialised 'fromIntegral'
i32_to_f32 :: Int32 -> Float
i32_to_f32 = fromIntegral

-- | Type-specialised 'fromIntegral' of 'fromEnum.
char_to_i32 :: Char -> Int32
char_to_i32 = fromIntegral . fromEnum

{- | 'p_lcg_glibc' scaled to (-1,1) with 'Char' seed.

> run (p_take 5 (p_lcg_white 'α')) == [0.0000061886385,-0.7430545,0.2597701,0.3011079,-0.5965075]
-}
p_lcg_white :: Monad m => Char -> P m (Is ()) Float
p_lcg_white = fmap (\x -> i32_to_f32 x / i32_to_f32 maxBound) . p_lcg_glibc . char_to_i32

{- | Random numbers in (-1.0,1.0).

> runT (p_take 5 p_white)
-}
p_white :: (MonadIO m, Random t, Fractional t) => P m (Is ()) t
p_white = p_map_m (\_ -> liftIO (randomRIO (-1.0, 1.0))) (p_inf ())

{- | Timestamp values in relation to indicated /t0/.

> time >>= \t0 -> runT (p_timestamp t0 (p_take 3 p_getc))
-}
p_timestamp :: MonadIO m => Time -> P m k t -> P m k (Time, t)
p_timestamp t0 m = m ~> repeatedly (await >>= \x -> liftIO time >>= \t -> yield (t - t0, x))

{- | Delay thread by indicated number of seconds at each step.

> runT (p_trace "x=" (p_pause_thread (p_from_list [0,1,2])))
-}
p_pause_thread :: (MonadIO m, RealFrac t) => P m k t -> P m k t
p_pause_thread = p_map_m_ (liftIO . pauseThread)

{- | Delay thread until indicated time (NTP) at each step.

> p t0 = fmap time_pp (p_pause_thread_until (p_from_list (map (+ t0) [0,1,3])))
> time >>= \t0 -> runT_ (p_trace "x=" (p t0))
-}
p_pause_thread_until :: MonadIO m => P m k Time -> P m k Time
p_pause_thread_until = p_map_m_ (liftIO . pauseThreadUntil)

-- | A timer that counts from /n/ in increments of /n/ seconds
p_timer_from :: (MonadIO m, RealFrac t) => t -> P m k t
p_timer_from n = unfoldT (\c -> liftIO (pauseThread n) >> return (Just (c, c + n))) n

p_cons :: Monad m => t -> P m k t -> P m k t
p_cons x = p_seq_2 (p_once x)

{- | A timer that counts from 0 in increments of /n/ seconds

> runT_ (p_trace "x=" (p_tuple_2 (p_counter 0) (p_timer 1.5)))
-}
p_timer :: (MonadIO m, RealFrac t) => t -> P m k t
p_timer = p_cons 0 . p_timer_from

-- | Pattern 'mapMaybe'
p_map_maybe :: Monad m => (t -> Maybe u) -> P m k t -> P m k u
p_map_maybe f p =
  let z = do
        x <- await
        forM_ (f x) yield
  in p ~> repeatedly z

{- | Pattern 'catMaybes'

> run (p_cat_maybes (p_from_list [Just 1,Nothing,Just 3])) == [1,3]
-}
p_cat_maybes :: Monad m => P m k (Maybe c) -> P m k c
p_cat_maybes = p_map_maybe id

{- | Mark immediately succesive duplicates with 'Nothing'.

> run (p_mark_successive_duplicates (p_from_list [1,2,2,3,3,3]))
-}
p_mark_successive_duplicates :: (Monad m, Eq t) => P m k t -> P m k (Maybe t)
p_mark_successive_duplicates p =
  p
    ~> auto
      ( unfoldMealy
          ( \x1 x0 ->
              if Just x0 == x1
                then (Nothing, x1)
                else (Just x0, Just x0)
          )
          Nothing
      )

{- | 'p_cat_maybes' of 'p_mark_successive_duplicates'.

> run (p_remove_successive_duplicates (p_from_list [1,1,2,2,3,3])) == [1,2,3]
> run (p_remove_successive_duplicates (p_from_list [1,2,3,1,2,3])) == [1,2,3,1,2,3]
-}
p_remove_successive_duplicates :: (Monad m, Eq t) => P m k t -> P m k t
p_remove_successive_duplicates = p_cat_maybes . p_mark_successive_duplicates

-- | Pattern 'iterate'.
p_iterate :: Monad m => (t -> t) -> t -> P m k t
p_iterate f x = p_inf () ~> auto (unfoldMoore (\i -> let r = f i in (i, const r)) x)

{- | Geometric series.

> run (p_geom 3 6 7) == [3,18,108,648,3888,23328,139968]
-}
p_geom :: (Monad m, Num t) => t -> t -> Int -> P m k t
p_geom i s n = p_take n (p_iterate (* s) i)

{- | Arithmetic series.

> run (p_series 3 2 7) == [3,5,7,9,11,13,15]
-}
p_series :: (Monad m, Num c) => c -> c -> Int -> P m k c
p_series i s n = p_take n (p_iterate (+ s) i)

{- | Counter from /n/.

> run (p_take 5 (p_counter 1)) == [1,2,3,4,5]
-}
p_counter :: (Monad m, Num t) => t -> P m k t
p_counter = p_iterate (+ 1)

{- | Drop first element of /x/.

> run (p_drop_1 (p_from_list "αβγ")) == "βγ"
-}
p_drop_1 :: Monad m => P m k t -> P m k t
p_drop_1 x = x ~> before echo await

{- | Count inputs starting at /k/.

> run (p_count_from 1 (p_from_list "αβγ")) == [1,2,3]
-}
p_count_from :: (Monad m, Num u) => u -> P m k t -> P m k u
p_count_from k p = p_drop_1 (p ~> scan (\n _ -> n + 1) (k - 1))

{- | Add input count starting at /k/.

> run (p_add_counter 1 (p_from_list "αβγ")) == [(1,'α'),(2,'β'),(3,'γ')]
-}
p_add_counter :: (Monad m, Num t) => t -> P m k u -> P m k (t, u)
p_add_counter k = p_tuple_2 (p_counter k)

{- | Intersperse /k/ in pattern /x/.

> run (p_intersperse 0 (p_from_list [1,2,3])) == [1,0,2,0,3]
-}
p_intersperse :: Monad m => t -> P m k t -> P m k t
p_intersperse k x = x ~> intersperse k

{- | Hold first value for length of pattern.

> run (p_hold_1 (p_from_list "αβγδεζ")) == "αααααα"
> run (p_hold_1 mempty) == []
-}
p_hold_1 :: Monad m => P m k t -> P m k t
p_hold_1 p =
  let f s =
        case s of
          Stop -> mempty
          Yield o r -> p_cons o (p_collect (const o) r)
          Await _ _ _ -> error "p_hold_1: await?"
  in stepMachine p f

-- > run (p_bool (p_from_list [1,1,0,0,1,0])) == [True,True,False,False,True,False]
p_bool :: (Monad m, Num t, Ord t) => P m k t -> P m k Bool
p_bool = fmap (> 0)

-- | Given 'Step', supply /e/ for 'Stop', apply /f/ at 'Yield', and 'error' on 'Await'.
x_step_yield :: r -> (t -> u -> r) -> Step k t u -> r
x_step_yield e f s =
  case s of
    Stop -> e
    Yield o r -> f o r
    Await _ _ _ -> error "x_step_yield: await?"

-- | Element-wise if, ie. if /k/ then Left /p/ else Right /q/.
p_switch1_2_either :: Monad m => P m k Bool -> P m k t -> P m k u -> P m k (Either t u)
p_switch1_2_either k p q =
  let s_f = x_step_yield mempty
      p_f k' = s_f (\o p' -> p_cons (Left o) (p_switch1_2_either k' p' q))
      q_f k' = s_f (\o q' -> p_cons (Right o) (p_switch1_2_either k' p q'))
      k_f = s_f (\o k' -> if o then stepMachine p (p_f k') else stepMachine q (q_f k'))
  in stepMachine k k_f

-- | Remove Either structure.
un_either :: Either t t -> t
un_either x =
  case x of
    Left r -> r
    Right r -> r

{- | 'un_either' of 'p_switch1_2_either'

> toP = p_from_list
> run (p_switch1_2 (p_bool (toP [1,1,0,0,1,0])) (toP "αβγ") (toP "δεζ")) == "αβδεγζ"
-}
p_switch1_2 :: Monad m => P m k Bool -> P m k t -> P m k t -> P m k t
p_switch1_2 k p q = fmap un_either (p_switch1_2_either k p q)

-- | Element-wise indexing at /k/ into /m/.  If /k/ is out of range insert /z/.
p_switch1_map :: Monad m => t -> P m k Int -> Map.Map Int (P m k t) -> P m k t
p_switch1_map z k m =
  let p_f ix k' =
        x_step_yield
          mempty
          (\r p' -> p_cons r (p_switch1_map z k' (Map.insert ix p' m)))
      k_f ix k' = case Map.lookup ix m of
        Just p -> stepMachine p (p_f ix k')
        Nothing -> p_cons z (p_switch1_map z k' m)
  in stepMachine k (x_step_yield mempty k_f)

{- | Element-wise indexing at /k/ into /l/.

> toP = p_from_list
> run (p_switch1 '.' (toP [1,1,0,2,2,-1,0]) (map toP ["αβ","γδ","εζ"])) == "γδαεζ.β"
-}
p_switch1 :: Monad m => t -> P m k Int -> [P m k t] -> P m k t
p_switch1 z k l = p_switch1_map z k (Map.fromList (zip [0 ..] l))

{- | If /k/ then /p/ else /q/.

> toP = p_from_list
> run (p_switch_2 (p_bool (toP [1,1,0,0,1,0])) (toP "αβ") (toP "γδε")) == "αβαβγδεγδεαβγδε"
-}
p_switch_2 :: Monad m => P m k Bool -> P m k t -> P m k t -> P m k t
p_switch_2 k p q =
  let k_f =
        x_step_yield
          mempty
          ( \o k' ->
              if o
                then p_seq_2 p (p_switch_2 k' p q)
                else p_seq_2 q (p_switch_2 k' p q)
          )
  in stepMachine k k_f

-- | Index at /k/ into /m/.  If /k/ is out of range insert /z/.
p_switch_map :: Monad m => t -> P m k Int -> Map.Map Int (P m k t) -> P m k t
p_switch_map z k m =
  let k_f =
        x_step_yield
          mempty
          ( \ix k' -> case Map.lookup ix m of
              Just p -> p_seq_2 p (p_switch_map z k' m)
              Nothing -> p_cons z (p_switch_map z k' m)
          )
  in stepMachine k k_f

{- | Index at /k/ into /l/.

> toP = p_from_list
> run (p_switch '.' (toP [1,1,0,2,1,0,-1,0]) (map toP ["αβ","γδ","εζ"])) == "γδγδαβεζγδαβ.αβ"
> run (p_switch 0 (toP [2,2,0,1]) (map toP [[1,2,3,1,2,3],[5,6],[8]])) == [8,8,1,2,3,1,2,3,5,6]
-}
p_switch :: Monad m => t -> P m k Int -> [P m k t] -> P m k t
p_switch z k l = p_switch_map z k (Map.fromList (zip [0 ..] l))
-}
