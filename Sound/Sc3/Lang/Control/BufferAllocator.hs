-- | Simple buffer allocator.
module Sound.Sc3.Lang.Control.BufferAllocator where

import Data.IORef {- base -}

import qualified Data.IntSet as IntSet {- containers -}

import Sound.Sc3.Server.Command.Plain {- hsc3 -}

data BufferAllocator = BufferAllocator (IORef IntSet.IntSet)

-- | Initialise BufferAllocator.
newBufferAllocator :: IO BufferAllocator
newBufferAllocator = fmap BufferAllocator (newIORef IntSet.empty)

{- | Allocate Buffer_Id.

> b <- newBufferAllocator
> allocBufferId b
-}
allocBufferId :: BufferAllocator -> IO Buffer_Id
allocBufferId (BufferAllocator bRef) = do
  let startIndex = 100
  b <- readIORef bRef
  if IntSet.null b
    then writeIORef bRef (IntSet.singleton startIndex) >> return startIndex
    else let nextId = IntSet.findMax b + 1 in writeIORef bRef (IntSet.insert nextId b) >> return nextId

{- | Free Buffer_Id.

> mapM_ (freeBufferId b) [100 .. 103]
-}
freeBufferId :: BufferAllocator -> Buffer_Id -> IO ()
freeBufferId (BufferAllocator bRef) k = do
  b <- readIORef bRef
  if IntSet.member k b
    then writeIORef bRef (IntSet.delete k b)
    else error "freeBufferId: unknown BufferId?"
