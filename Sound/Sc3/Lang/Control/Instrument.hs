-- | An instrument abstraction and a /default/ instrument for patterns.
module Sound.Sc3.Lang.Control.Instrument where

import Sound.Sc3 {- hsc3 -}

-- | An 'Instr' is either a 'Synthdef' or the 'String' naming a 'Synthdef'.
data Instr
  = Instr_Def {i_def :: Synthdef, i_send_release :: Bool}
  | Instr_Ref {i_ref :: String, i_send_release :: Bool}
  deriving (Eq, Show)

-- | All 'Instr' have a name.
i_name :: Instr -> String
i_name i =
  case i of
    Instr_Def s _ -> synthdefName s
    Instr_Ref nm _ -> nm

-- | All 'Instr' may have a 'Synthdef'.
i_synthdef :: Instr -> Maybe Synthdef
i_synthdef i =
  case i of
    Instr_Def s _ -> Just s
    Instr_Ref _ _ -> Nothing

-- | If 'Instr_Def' subsequent are 'Instr_Ref', else all 'Instr_Ref'.
i_repeat :: Instr -> [Instr]
i_repeat i =
  case i of
    Instr_Def d sr -> i : repeat (Instr_Ref (synthdefName d) sr)
    Instr_Ref _ _ -> repeat i

-- | 'Instr' of 'defaultSynthdef'.
defaultInstr :: Instr
defaultInstr = Instr_Def defaultSynthdef True

{-
withSc3 (send (d_recv defaultSynthdef))
-}
