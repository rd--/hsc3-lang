-- | An alternate interface to the functions at "Sound.Sc3.Common.Math.Warp"
module Sound.Sc3.Lang.Math.Warp where

import qualified Numeric {- base -}

import qualified Sound.Sc3.Common.Math.Warp as Sc3 {- hsc3 -}

-- | Warp direction.  'W_Map' is forward, 'W_Unmap' is reverse.
data W_Direction = W_Map | W_Unmap
  deriving (Eq, Enum, Bounded, Show)

-- | Warp type
type Warp t = W_Direction -> t -> t

-- | Forward warp.
w_map :: Warp t -> t -> t
w_map w = w W_Map

-- | Reverse warp.
w_unmap :: Warp t -> t -> t
w_unmap w = w W_Unmap

{- | A linear real value map.

> w = LinearWarp(ControlSpec(1,2))
> [0,0.5,1].collect{|n| w.map(n)} == [1,1.5,2]

>>> map (w_map (warpLinear 1 2)) [0,1/2,1] == [1,3/2,2]
True

>>> map (warpLinear (-1) 1 W_Map) [0,1/2,1] == [-1,0,1]
True
-}
warpLinear :: (Fractional a) => a -> a -> Warp a
warpLinear l r d = (if d == W_Map then Sc3.warp_lin else Sc3.warp_lin_inv) l r

{- | The left and right must both be non zero and have the same sign.

> w = ExponentialWarp(ControlSpec(1,2))
> [0,0.5,1].collect{|n| w.map(n)} == [1,pow(2,0.5),2]

>>> map (warpExponential 1 2 W_Map) [0,0.5,1] == [1,2 ** 0.5,2]
True

> import Sound.Sc3.Plot
> plot_p1_ln [map (warpExponential 1 2 W_Map) [0,0.01 .. 1]]
-}
warpExponential :: (Floating a) => a -> a -> Warp a
warpExponential l r d = (if d == W_Map then Sc3.warp_exp else Sc3.warp_exp_inv) l r

{- | Cosine warp

> w = CosineWarp(ControlSpec(1,2))
> [0,0.25,0.5,0.75,1].collect{|n| w.map(n)}

>>> map (warpCosine 1 2 W_Map) [0,0.25,0.5,0.75,1]
[0.0,0.1464466094067262,0.49999999999999994,0.8535533905932737,1.0]

> plot_p1_ln [map (warpCosine 1 2 W_Map) [0,0.01 .. 1]]
-}
warpCosine :: (Floating a) => a -> a -> Warp a
warpCosine l r d = (if d == W_Map then Sc3.warp_cos else Sc3.warp_cos_inv) l r

{- | Sine warp

>>> map (warpSine 1 2 W_Map) [0,0.25,0.5,0.75,1]
[0.0,0.3826834323650898,0.7071067811865475,0.9238795325112867,1.0]

> plot_p1_ln [map (warpSine 1 2 W_Map) [0,0.01 .. 1]]
-}
warpSine :: (Floating a) => a -> a -> Warp a
warpSine l r d = (if d == W_Map then Sc3.warp_sin else Sc3.warp_sin_inv) l r

{- | Fader warp.  Left and right values are ordinarily zero and one.

>>> map (warpFader 0 1 W_Map) [0,0.5,1] == [0,0.25,1]
True

> plot_p1_ln [map (warpFader 0 2 W_Map) [0,0.01 .. 1]]
> plot_p1_ln [map (warpFader 0 1 W_Unmap . warpFader 0 1 W_Map) [0,0.01 .. 1]]
-}
warpFader :: Floating a => a -> a -> Warp a
warpFader l r d = (if d == W_Map then Sc3.warp_amp else Sc3.warp_amp_inv) l r

{- | DB fader warp. Left and right values are ordinarily negative infinity and zero.
An input of @0@ gives @-180@.

>>> map (round . warpDbFader (-180) 0 W_Map) [0,0.5,1] == [-180,-12,0]
True

> plot_p1_ln [map (warpDbFader (-60) 0 W_Map) [0,0.01 .. 1]]
> plot_p1_ln [map (warpDbFader 0 60 W_Unmap) [0 .. 60]]
-}
warpDbFader :: (Eq a, Floating a) => a -> a -> Warp a
warpDbFader l r d = (if d == W_Map then Sc3.warp_db else Sc3.warp_db_inv) l r

{- | A curve warp given by a real /n/.

>>> w_map (warpCurve (-3) 1 2) 0.25
1.5552791692202022

>>> w_map (warpCurve (-3) 1 2) 0.50
1.8175744761936437

> plot_p1_ln [map (warpCurve (-3) 1 2 W_Map) [0,0.01 .. 1]]
> plot_p1_ln (map (\c -> map (warpCurve c 1 2 W_Map) [0,0.01 .. 1]) [0,3,6,9])
> plot_p1_ln [map (warpCurve 7 20 20000 W_Unmap . warpCurve 7 20 20000 W_Map) [0,0.01 .. 1]]
-}
warpCurve :: (Ord a, Floating a) => a -> a -> a -> Warp a
warpCurve k l r d = (if d == W_Map then Sc3.warp_curve else Sc3.warp_curve_inv) k l r

{- | Select warp function by name.  Numerical names are interpreted as /curve/ values for 'warpCurve'.

> let Just w = warpNamed "lin"
> let Just w = warpNamed "-3"
> let Just w = warpNamed "6"
> plot_p1_ln [map (w 1 2 W_Map) [0,0.01 .. 1]]
-}
warpNamed :: (Ord a, RealFrac a, Floating a) => String -> Maybe (a -> a -> Warp a)
warpNamed nm =
  case nm of
    "lin" -> Just warpLinear
    "exp" -> Just warpExponential
    "sin" -> Just warpSine
    "cos" -> Just warpCosine
    "amp" -> Just warpFader
    "db" -> Just warpDbFader
    _ -> case Numeric.readSigned Numeric.readFloat nm of
      [(c, "")] -> Just (warpCurve c)
      _ -> Nothing
