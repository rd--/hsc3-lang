-- | Text lookup for math.
module Sound.Sc3.Lang.Math.Named where

-- | Lookup unary Num method by name.
unaryNumLookup :: Num t => String -> Maybe (t -> t)
unaryNumLookup x =
  case x of
    "abs" -> Just abs
    "negate" -> Just negate
    "signum" -> Just signum
    _ -> Nothing

-- | Lookup binary Num method by name.
binaryNumLookup :: Num t => String -> Maybe (t -> t -> t)
binaryNumLookup x =
  case x of
    "+" -> Just (+)
    "-" -> Just (-)
    "*" -> Just (*)
    _ -> Nothing

-- | Lookup unary Floating method by name.
unaryFloatingLookup :: Floating t => String -> Maybe (t -> t)
unaryFloatingLookup x =
  case x of
    "exp" -> Just exp
    "log" -> Just log
    "sqrt" -> Just sqrt
    "sin" -> Just sin
    "cos" -> Just cos
    "tan" -> Just tan
    "asin" -> Just asin
    "acos" -> Just acos
    "atan" -> Just atan
    "sinh" -> Just sinh
    "cosh" -> Just cosh
    "tanh" -> Just tanh
    "asinh" -> Just asinh
    "acosh" -> Just acosh
    "atanh" -> Just atanh
    _ -> Nothing

-- | Lookup binary Floating method by name.
binaryFloatingLookup :: Floating t => String -> Maybe (t -> t -> t)
binaryFloatingLookup x =
  case x of
    "**" -> Just (**)
    "logBase" -> Just logBase
    _ -> Nothing
