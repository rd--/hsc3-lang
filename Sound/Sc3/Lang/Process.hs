-- | Functions for interacting with an sclang process.
module Sound.Sc3.Lang.Process where

import System.Process {- process -}

-- | Run sclang -v and parse result string.
sclang_version :: IO String
sclang_version = do
  txt <- readProcess "sclang" ["-v"] ""
  case words txt of
    "sclang" : v : _ -> return v
    _ -> error "sclang_version?"
