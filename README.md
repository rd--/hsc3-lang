hsc3-lang
---------

hsc3-lang provides `Sound.Sc3.Lang`, a
[Haskell](http://haskell.org/)
module that defines a subset of functions from the
[SuperCollider Language](http://audiosynth.com/)
class library.

debian: hmatrix = libgsl-dev liblapack-dev libatlas-base-dev

## cli

[hsc3-lang](?t=hsc3-lang&e=md/lang.md)

© [rohan drape](http://rohandrape.net/),
  2007-2024,
  [gpl-3](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 348  Tried: 348  Errors: 0  Failures: 0
$
```
