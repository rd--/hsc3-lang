Cabal-Version:     2.4
Name:              hsc3-lang
Version:           0.22
Synopsis:          Haskell SuperCollider Language
Description:       Haskell library defining operations from the
                   SuperCollider language class library
License:           GPL-3.0-only
Category:          Sound
Copyright:         (c) Rohan Drape, 2007-2024
Author:            Rohan Drape
Maintainer:        rd@rohandrape.net
Stability:         Experimental
Homepage:          http://rohandrape.net/t/hsc3-lang
Tested-With:       GHC == 9.12.1
Build-Type:        Simple

Data-files:        README.md

Library
  Build-Depends:   base >= 4.8 && < 5,
                   bytestring,
                   containers,
                   data-ordlist,
                   directory,
                   dlist,
                   filepath,
                   hashable,
                   hmt-base == 0.22.*,
                   hosc == 0.22.*,
                   hsc3 == 0.22.*,
                   MonadRandom,
                   process,
                   transformers,
                   split,
                   random,
                   random-shuffle,
                   transformers
  GHC-Options:     -Wall -Wno-x-partial -Wno-incomplete-uni-patterns
  Exposed-modules: Sound.Sc3.Lang.Collection
                   Sound.Sc3.Lang.Collection.Extension
                   Sound.Sc3.Lang.Collection.Numerical.Extending
                   Sound.Sc3.Lang.Collection.Numerical.Truncating
                   Sound.Sc3.Lang.Control.BufferAllocator
                   Sound.Sc3.Lang.Control.Duration
                   Sound.Sc3.Lang.Control.Event
                   Sound.Sc3.Lang.Control.Instrument
                   Sound.Sc3.Lang.Control.Pitch
                   Sound.Sc3.Lang.Control.OverlapTexture
                   Sound.Sc3.Lang.Control.Spec
                   Sound.Sc3.Lang.Core
                   Sound.Sc3.Lang.Math
                   Sound.Sc3.Lang.Math.Statistics
                   Sound.Sc3.Lang.Math.Warp
                   Sound.Sc3.Lang.Pattern
                   Sound.Sc3.Lang.Pattern.Bind
                   Sound.Sc3.Lang.Pattern.List
                   Sound.Sc3.Lang.Pattern.P
                   Sound.Sc3.Lang.Pattern.P.Base
                   Sound.Sc3.Lang.Pattern.P.Core
                   Sound.Sc3.Lang.Pattern.P.Event
                   Sound.Sc3.Lang.Pattern.P.Sc3
                   Sound.Sc3.Lang.Pattern.Plain
                   Sound.Sc3.Lang.Pattern.Stream
                   Sound.Sc3.Lang.Process
                   Sound.Sc3.Lang.Random.Lorrain_1980
                   Sound.Sc3.Lang.Random.Gen
                   Sound.Sc3.Lang.Random.Id
                   Sound.Sc3.Lang.Random.Io
                   Sound.Sc3.Lang.Random.Monad
                   Sound.Sc3.Lang.Random.Server
  default-language:Haskell2010

executable hsc3-lang
  build-depends:    base == 4.* && < 5,
                    filepath,
                    hsc3 == 0.22.*,
                    process,
                    split
  hs-source-dirs:   cmd
  main-is:          lang.hs
  default-language: Haskell2010
  ghc-options:      -Wall -fwarn-tabs

Source-Repository  head
  Type:            git
  Location:        https://gitlab.com/rd--/hsc3-lang
