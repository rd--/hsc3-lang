# hsc3-lang

## scsyndef-to-texture (October, 2020)

Read a `.scsyndef` (SynthDef) file, reconstruct a `UGen` value, delete
the root `Out` node (or variant) and then run one of the `Texture`
functions (`overlap`, `spawn` or `xfade`) at the resulting UGen.
Parameters are given as a comma separated string, fields are as at:
[OverlapTexture.hs](?t=hsc3-lang&e=Sound/SC3/Lang/Control/OverlapTexture.hs).
The number of repetitions may be given as `inf` (for infinite).  Once
the texture thread is started the process waits for a character at
`stdin` and exits when it receives one.

~~~~
$ hsc3-lang scsyndef-to-texture f2d6c26e3daa1d03.scsyndef overlap,0.25,0.5,5,inf
Q
$
~~~~

This requires that `runhaskell` is available and that it can access
`hsc3-lang` library and it's dependencies.
