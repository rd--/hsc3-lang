import Data.List.Split {- split -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}
import Text.Printf {- base -}

import qualified Sound.Sc3.Server.Graphdef.Binary as Graphdef {- hsc3 -}
import qualified Sound.Sc3.Server.Graphdef.Read as Read {- hsc3 -}
import qualified Sound.Sc3.Ugen.Graph.Reconstruct as Reconstruct {- hsc3 -}

{- | Syndef to texture

> let sy = "/home/rohan/sw/hsc3-graphs/db/0074778a54a5c42a.scsyndef"
> scsyndef_to_texture sy (words "overlap 2 1 4 inf")
-}
scsyndef_to_texture :: FilePath -> [String] -> IO ()
scsyndef_to_texture sy_nm param = do
  gr <- Graphdef.read_graphdef_file sy_nm
  let to_name :: String -> String
      to_name = ("gr_" ++) . map (\c -> if c `elem` "-." then '_' else c)
      nm = to_name (dropExtension (takeFileName sy_nm))
      (_, gr') = Read.graphdef_to_graph gr
      hs = Reconstruct.reconstruct_graph_module nm gr'
      inf x = if x == "inf" then "maxBound" else x
      txt_str =
        case param of
          ["overlap", d1, d2, d3, d4] -> printf "O.overlapTextureU (%s,%s,%s,%s)" d1 d2 d3 (inf d4)
          ["spawn", d1, d2] -> printf "O.spawnTextureU (const %s,%s)" d1 (inf d2)
          ["xfade", d1, d2, d3] -> printf "O.xfadeTextureU (%s,%s,%s)" d1 d2 (inf d3)
          _ -> error "scsyndef_to_texture?"
      imp =
        [ "import Control.Concurrent {- base -}"
        , "import System.Exit {- base -}"
        , "import System.IO {- base -}"
        , "import qualified Sound.Sc3.Ugen.Analysis as Analysis {- hsc3 -}"
        , "import qualified Sound.Sc3.Lang.Control.OverlapTexture as O {- hsc3-lang -}"
        ]
      mn =
        [ "main :: IO ()"
        , "main = do"
        , "  let txt = " ++ txt_str ++ " (snd (Analysis.ugen_remove_out_node " ++ nm ++ "))"
        , "  th <- forkFinally txt (const exitSuccess)"
        , "  hSetBuffering stdin NoBuffering"
        , "  _ch <- getChar"
        , "  killThread th"
        ]
      hs_nm = "/tmp/scsyndef-to-texture-" ++ nm ++ ".hs"
  writeFile hs_nm (unlines (imp ++ hs ++ mn))
  _ <- system ("runhaskell " ++ hs_nm)
  return ()

usage :: [String]
usage =
  [ "hsc3-lang cmd [arg]"
  , "  scsyndef-to-texture scsyndef-file texture-param"
  ]

main :: IO ()
main = do
  arg <- getArgs
  case arg of
    ["scsyndef-to-texture", sy_nm, param] -> scsyndef_to_texture sy_nm (splitOn "," param)
    _ -> putStrLn (unlines usage)
