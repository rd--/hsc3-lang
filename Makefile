all:
	echo "hsc3-lang"

mk-cmd:
	echo "hsc3-lang - NIL"

clean:
	rm -fR dist dist-newstyle *~
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hsc3-lang

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound

extra-dep:
	cabal v1-install hmatrix-special machines
